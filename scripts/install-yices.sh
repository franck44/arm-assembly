#!/bin/bash

YICES_VERSION=$1
FILE="yices-$YICES_VERSION-x86_64-pc-linux-gnu-static-gmp.tar.gz"
DIR="./solvers/yices/$YICES_VERSION"

echo ">>>>> Downloading Yices $YICES_VERSION <<<<<"
wget --no-check-certificate  "http://yices.csl.sri.com/releases/$YICES_VERSION/$FILE"
tar -xzf "$FILE"

mkdir -p "./solvers/yices"
mv -f "yices-$YICES_VERSION" "$DIR"

rm "$FILE"

echo ">>>>> Linking Yices $YICES_VERSION <<<<<"
export PATH="$PATH:$DIR/bin/"

echo ">>>>> Installed Yices <<<<<"
yices-smt2 --version
