#!/bin/bash

Z3_VERSION=$1
FILE="z3-$Z3_VERSION-x64-ubuntu-14.04.zip"
DIR="./solvers/z3/$Z3_VERSION"

echo ">>>>> Downloading Z3 $Z3_VERSION <<<<<"
wget --no-check-certificate  "https://github.com/Z3Prover/z3/releases/download/z3-$Z3_VERSION/$FILE"
unzip "$FILE"

mkdir -p "./solvers/z3"
mv -f "$(basename "$FILE" .zip)/" "$DIR"

rm "$FILE"

echo ">>>>> Linking Z3 $Z3_VERSION <<<<<"
export PATH="$PATH:$DIR/bin/"

echo ">>>>> Installed Z3 <<<<<"
z3 --version
