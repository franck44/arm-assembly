#!/bin/bash

CVC4_VERSION=$1
FILE="cvc4-$CVC4_VERSION-x86_64-linux-opt"
DIR="./solvers/cvc4/$CVC4_VERSION"

echo ">>>>> Downloading CVC4 $CVC4_VERSION <<<<<"
wget --no-check-certificate  "http://cvc4.cs.stanford.edu/downloads/builds/x86_64-linux-opt/$FILE"
chmod u+x "$FILE"

mkdir -p "$DIR"
mv "$FILE" "$DIR/cvc4"

echo ">>>>> Linking CVC4 $CVC4_VERSION <<<<<"
export PATH="$PATH:$DIR/"

echo ">>>>> Installed CVC4 <<<<<"
cvc4 --version
