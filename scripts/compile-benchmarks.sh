#!/bin/bash
set -e

SCRIPT_FILE=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_FILE")
ROOT_DIR="$SCRIPT_DIR/.."

BENCHMARK_DIR="$ROOT_DIR/src/test/resources/benchmarks"
SETUP_DIR="$BENCHMARK_DIR/setup"

OPT_LEVELS=("0" "1" "2" "3")

cd "$BENCHMARK_DIR"

echo ">>>>> Compiling the bootloader       <<<<<"
arm-none-eabi-as -mcpu=arm926ej-s -g "$SETUP_DIR/startup.s" -o "$SETUP_DIR/startup.o"

for dir in `find -mindepth 2 -type d`
do
  echo ">>>>> examining directory $dir       <<<<<"
  echo "----------------------------------------------------------"
  cd "$dir"

  # Look for C source code to compile
  if test -n "$(find . -maxdepth 1 -name '*.c' -print -quit)"
  then
    for level in ${OPT_LEVELS[*]}
    do
      echo ">>>>> Compiling with level ${level}  <<<<<"

      echo ">>>>> Removing previous files        <<<<<"
      rm -rf *.o *.elf *.bin compiled.asm "compiled.o$level.asm"

      echo ">>>>> Compiling the example          <<<<<"
      arm-none-eabi-gcc -fomit-frame-pointer -lm -O$level -c -mcpu=arm926ej-s -g *.c -o "compiled.o$level.o"

      echo ">>>>> Linking bootloader and program <<<<<"
      arm-none-eabi-gcc -T "$SETUP_DIR/test.ld" "$SETUP_DIR/startup.o" "compiled.o$level.o" -lm -nostartfiles -o "compiled.o$level.elf"

      echo ">>>>> Extracting the binary format   <<<<<"
      arm-none-eabi-objcopy -O binary "compiled.o$level.elf" "compiled.o$level.bin"

      echo ">>>>> Dumping the textual form       <<<<<"
      arm-none-eabi-objdump -d "compiled.o$level.elf" > "compiled.o$level.asm"

      echo ">>>>> Cleaning up                    <<<<<"
      rm -rf *.o *.elf *.bin
    done
  else
    echo ">>>>> No C source code found.        <<<<<"
  fi

  echo "=========================================================="
  echo ""
  cd "$BENCHMARK_DIR"
done
