package au.edu.mq.comp.assembly
package controlflow

import au.edu.mq.comp.assembly.semantics.DataFlowAnalysis
import au.edu.mq.comp.assembly.smt.Solving.isSat
import au.edu.mq.comp.assembly.smt.Syntax._
import au.edu.mq.comp.assembly.smt.{FreeVariables, SmtEncoder, SolverContext, VariableMap}

import org.bitbucket.franck44.automat.auto.NFA
import org.bitbucket.franck44.automat.edge.Implicits._
import org.bitbucket.franck44.automat.edge.LabDiEdge
import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.{QualifiedId, Term}

import scala.util.{Failure, Success}

/**
  * Builds automata from traces and inductive state predicate sequences.
  *
  * @tparam I The type of instructions.
  */
case class PredicateAutomatonBuilder[I: InstructionSet]()(implicit smtCtx: SolverContext) extends Logging {

  private val smtLogic = implicitly[InstructionSet[I]].machine.smtLogic
  require(smtCtx.supportsLogic(smtLogic), s"Solver does not support logic $smtLogic.")

  /**
    * Creates an automaton from the given trace.
    * The automaton describes a regular language guaranteed to contain the given trace,
    * as well as possibly other traces for which an inductive sequence of state predicates
    * can be constructed from the given sequence.
    *
    * @param trace         The trace from which the automaton is created.
    * @param predicates    An inductive sequence of state predicates for the trace.
    *                      Its length must be trace.length + 1.
    * @param parallel      An optional sequence of instructions of parallel traces.
    *                      If they satisfy the conditions of inductive sequences (checked), they are added to the automaton.
    *                      The length of this sequence must be equal to the length of the trace.
    * @param alphabet      A set of instructions the automaton can use. This is used to add self-loops to states where possible.
    * @param redundantEdge A predicate indicating a given edge is redundant. If so, no checks to ensure its safety will be performed.
    */
  def buildPredicateAutomaton(trace: Seq[I], predicates: Seq[StatePredicate])(
    parallel: Seq[Set[I]] = List.fill(trace.length)(Set.empty),
    alphabet: Set[I] = Set.empty,
    redundantEdge: (Term, I, Term) => Boolean = (_, _, _) => false
  ): NFA[Term, I] = {

    require(trace.length + 1 == predicates.length, "Incorrect number of predicates.")
    require(trace.length == parallel.length, "Incorrect number of parallel instruction edges.")

    // Forward edges along the trace.
    val forwardEdges = for (i <- trace.indices)
      yield (predicates(i).aTerm ~> predicates(i + 1).aTerm) (trace(i))

    // Parallel edges for similar traces.
    val parallelEdges = computeSafeParallelEdges(predicates, parallel, redundantEdge)

    // Back-edges to create loops.
    val backEdges = computeSafeBackEdges(predicates, parallel.zip(trace).map { case (p, i) => p + i }, redundantEdge)

    // Self-loops for irrelevant instructions.
    val selfLoops = computeSafeSelfLoops(predicates, alphabet, redundantEdge)

    val automaton = NFA(Set(predicates.head.aTerm), (forwardEdges ++ parallelEdges ++ backEdges ++ selfLoops).toSet, Set(predicates.last.aTerm))
    logPredicateAutomaton("predicate automaton:", automaton)

    automaton
  }

  private def computeSafeParallelEdges(predicates: Seq[StatePredicate], parallel: Seq[Set[I]], redundantEdge: (Term, I, Term) => Boolean): Seq[LabDiEdge[Term, I]] = {
    for {
      i <- parallel.indices

      p1 = predicates(i)
      p2 = predicates(i + 1)

      instruction <- parallel(i)

      if !redundantEdge(p1.aTerm, instruction, p2.aTerm)
      if checkTrivialSelfLoop(p1, instruction, p2) || checkPost(p1, instruction, p2)
    } yield (p1.aTerm ~> p2.aTerm) (instruction)
  }

  private def computeSafeBackEdges(predicates: Seq[StatePredicate], instructions: Seq[Set[I]], redundantEdge: (Term, I, Term) => Boolean): Seq[LabDiEdge[Term, I]] = {
    for {
      i <- instructions.indices
      j <- instructions.indices

      // No sense in jumping ahead
      if i < j

      // Avoid re-adding forward edges
      if predicates(i + 1).aTerm != predicates(j + 1).aTerm

      // Only add edges for repeated instructions
      iInstr = instructions(i)
      jInstr = instructions(j)
      instruction <- iInstr.intersect(jInstr)

      // Check if edge is legal (and not redundant).
      p1 = predicates(j)
      p2 = predicates(i + 1)
      if !redundantEdge(p1.aTerm, instruction, p2.aTerm)
      if checkTrivialSelfLoop(p1, instruction, p2) || checkPost(p1, instruction, p2)

    } yield (p1.aTerm ~> p2.aTerm) (instruction)
  }

  private def computeSafeSelfLoops(predicates: Seq[StatePredicate], instructions: Set[I], redundantEdge: (Term, I, Term) => Boolean): Set[LabDiEdge[Term, I]] = {
    for {
      p <- predicates.toSet[StatePredicate]
      i <- instructions

      if !redundantEdge(p.aTerm, i, p.aTerm)
      if checkTrivialSelfLoop(p, i, p)
    } yield (p.aTerm ~> p.aTerm) (i)
  }

  /**
    * Performs an efficient syntactic check to determine if an instruction trivially upholds a given predicate.
    *
    * @return True if the two predicates are equal and the instruction does not modify any of their free variables.
    */
  private def checkTrivialSelfLoop(pre: StatePredicate, instruction: I, post: StatePredicate): Boolean = checkTrivialSelfLoop(pre.aTerm, instruction, post.aTerm)

  private val checkTrivialSelfLoop = memoize { (pre: Term, instruction: I, post: Term) =>
    lazy val relevant = freeVariables(pre)
    lazy val modified = modifiedVariables(instruction)
    pre == post && (relevant & modified).isEmpty
  }

  private def freeVariables(expr: Term): Set[QualifiedId] = memoize(FreeVariables.compute(_: Term))(expr)

  private def modifiedVariables(instruction: I): Set[QualifiedId] = memoize(DataFlowAnalysis.definedVariables(_: I).toSet.map(_.aTerm.qualifiedId))(instruction)

  /**
    * Checks if the strongest postcondition of pre and instruction implies post.
    *
    * @return True if the implication always holds, false otherwise.
    */
  private def checkPost(pre: StatePredicate, instruction: I, post: StatePredicate): Boolean = {
    val preState = VariableMap.initial
    val (effect, postState) = SmtEncoder.encodeInstruction(instruction, preState)

    logger.debug(s"(Unindexed) precondition: ${pre.show}")
    val before = preState.substitute(pre)
    logger.debug(s"Indexed precondition: ${before.show}")

    logger.debug(s"(Unindexed) postcondition: ${post.show}")
    val after = postState.substitute(post)
    logger.debug(s"Indexed postcondition: ${after.show}")

    val formula = before && effect && !after
    if (formula == False())
      true
    else if (formula == True())
      false
    else
      isSat(Set(formula), smtLogic) match {
        case Success(sat) => !sat
        case Failure(e)   =>
          // TODO: In general, a failure may be acceptable here (just return false). But in order to catch bugs during development, be strict and throw.
          //logger.error("Failed to check implication", e)
          //false
          throw e
      }
  }
}