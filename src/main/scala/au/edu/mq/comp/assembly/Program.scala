package au.edu.mq.comp.assembly

/**
  * Represents a binary program.
  *
  * @param initialLocation        The initial location at which execution must always commence.
  * @param instructionAt          A map from program locations to instructions.
  * @param initialStatePredicates A set of state predicates constraining valid initial states for program executions.
  * @tparam I The type of instructions in the binary program.
  */
case class Program[I](initialLocation: Location, instructionAt: Map[Location, I], initialStatePredicates: Set[StatePredicate]) {
  /**
    * Converts a sequence of program locations to the corresponding trace of instructions.
    *
    * @param locationTrace A sequence of valid program locations.
    * @return A trace of instructions of the same length.
    */
  def materializeTrace(locationTrace: Seq[Location]): Seq[I] = locationTrace.map(instructionAt)

  /**
    * Converts a sequence of program locations to the corresponding trace of instructions, labeled with their location.
    *
    * @param locationTrace A sequence of valid program locations.
    * @return A trace of pairs of program locations and instructions, of the same length as the input sequence.
    */
  def materializeLabeledTrace(locationTrace: Seq[Location]): LabeledTrace[I] = locationTrace.map { l => (l, instructionAt(l)) }

  /**
    * The set of all instructions present in the program.
    */
  lazy val instructions: Set[I] = instructionAt.values.toSet

  /**
    * The set of all pairs of locations and corresponding instructions in the program.
    */
  lazy val labeledInstructions: Set[(Location, I)] = instructionAt.toSet

  /**
    * Checks if the given location is a valid program location.
    */
  def containsLocation(location: Location): Boolean = instructionAt.isDefinedAt(location)
}
