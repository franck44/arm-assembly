package au.edu.mq.comp.assembly

trait TypedSet[A[_]] {

  import au.edu.mq.comp.assembly.TypedMap.Void

  def contains[T](x: A[T]): Boolean = toSet.contains(x)

  /**
    * Invokes the given function at least once on every member.
    */
  def iterate(fct: TypedFunction[A, Void]): Unit

  def map[B[_]](f: TypedFunction[A, B]): TypedSet[B] = {
    var set = TypedSet.empty[B]
    iterate(new TypedFunction[A, Void] {
      override def apply[I](x: A[I]): Void[I] = set = set + f(x)
    })
    set
  }

  lazy val toSet: Set[A[_]] = {
    var set = Set.empty[A[_]]
    iterate(new TypedFunction[A, Void] {
      override def apply[I](x: A[I]): Void[I] = set = set + x
    })
    set
  }

  /**
    * Unions two sets.
    */
  def ++(other: TypedSet[A]): TypedSet[A] = fct => {
    iterate(fct)
    other.iterate(fct)
  }

  /**
    * Adds an element to the set.
    */
  def +[T](x: A[T]): TypedSet[A] = this ++ TypedSet(x)
}

object TypedSet {
  /**
    * The empty set.
    */
  def empty[A[_]]: TypedSet[A] = _ => {}

  /**
    * Creates a singleton set.
    */
  def apply[A[_], T](x: A[T]): TypedSet[A] = fct => fct(x)
}
