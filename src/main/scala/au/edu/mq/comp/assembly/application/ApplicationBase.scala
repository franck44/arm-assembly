package au.edu.mq.comp.assembly
package application

abstract class ApplicationBase[I, C: InstructionSet](args: Array[String]) {

  import au.edu.mq.comp.assembly.controlflow.resolution.{InterpolantsSuccessorResolution, SuccessorResolution, UniqueSuccessorResolution, WpSuccessorResolution}
  import au.edu.mq.comp.assembly.controlflow.{ControlFlowGraph, PredicateAutomatonBuilder, Reconstruction, UniqueSuccessorAnalysis}
  import au.edu.mq.comp.assembly.smt._

  import scala.util.Try

  import java.io.File

  protected lazy val config = new Config(args)

  // ***************************
  //  Files to analyze.
  // ***************************

  protected lazy val syntax: String = if (config.regexPath())
    "regex"
  else
    "glob"

  protected lazy val allFiles: Seq[File] = {
    import java.nio.file._
    import java.nio.file.attribute.BasicFileAttributes

    val matcher = FileSystems.getDefault.getPathMatcher(s"$syntax:${config.input()}")

    var files: List[File] = Nil

    val root = Paths.get("")

    Files.walkFileTree(root, new SimpleFileVisitor[Path] {
      override def visitFile(path: Path, basicFileAttributes: BasicFileAttributes): FileVisitResult = {
        if (matcher.matches(path))
          files = path.toFile :: files
        FileVisitResult.CONTINUE
      }
    })

    files.reverse
  }

  protected def parse(file: File): Try[Program[I]]

  // ***************************
  //  Building reconstruction.
  // ***************************

  protected def concretize: I => Set[C]

  protected def uniqueAnalysis: UniqueSuccessorAnalysis[C]

  protected val buildContext: (String, SmtMode) => SolverContext = memoize { (solverName, usageMode) =>
    import org.bitbucket.franck44.scalasmt.configurations.AppConfig

    val config = AppConfig.config.find(_.name == solverName).get
    usageMode match {
      case SmtMode.Fresh => FreshSolver(config)
      case SmtMode.Reuse => SolverCache(config)
    }
  }

  protected lazy val collector: ValueCollector =
    ValueCollector()(buildContext(config.smtValue(), config.smtmodeValue()))
  protected lazy val automatonBuilder: PredicateAutomatonBuilder[(Location, C)] =
    PredicateAutomatonBuilder()(implicitly, buildContext(config.smtPredicate(), config.smtmodePredicate()))

  protected lazy val traceEncoder: TraceEncoder[I, (Location, C)] = {
    config.traceEncoder().foldLeft(None: Option[TraceEncoder[I, (Location, C)]]) {
      case (None, TraceEncoderType.Default)        => Some(DefaultTraceEncoder())
      case (Some(x), TraceEncoderType.Simple)      => Some(SimpleProjectingTraceEncoder(x, implicitly[InstructionSet[C]].machine.pcExpr))
      case (Some(x), TraceEncoderType.SSA)         => Some(SSAOnlyTraceEncoder(x)(buildContext(config.smtSSA(), config.smtmodeSSA())))
      case (Some(x), TraceEncoderType.Core)        => Some(UnsatCoreSSATraceEncoder(x)(buildContext(config.smtCore(), config.smtmodeCore())))
      case (Some(x), TraceEncoderType.Constrained) => Some(ConstrainedSSATraceEncoder(x)(implicitly, buildContext(config.smtConstrained(), config.smtmodeConstrained())))
      case _                                       => throw new IllegalArgumentException("Invalid trace encoder configuration.")
    }.getOrElse(throw new IllegalArgumentException("Invalid trace encoder configuration."))
  }

  protected lazy val resolution: SuccessorResolution[I, C] = if (config.wp())
    WpSuccessorResolution[I, C](collector, traceEncoder, automatonBuilder, config.simplifierWp())
  else
    InterpolantsSuccessorResolution[I, C](collector, traceEncoder, automatonBuilder)(smtCtx = buildContext(config.smtItp(), config.smtmodeItp()), instructionSet = implicitly)
  protected lazy val (actualResolution, actualUniqueAnalysis): (SuccessorResolution[I, C], UniqueSuccessorAnalysis[C]) = if (config.optimize())
    (resolution, uniqueAnalysis)
  else
    (UniqueSuccessorResolution(uniqueAnalysis, automatonBuilder) +> resolution, UniqueSuccessorAnalysis.Dummy())

  protected lazy val reconstruction: Reconstruction[I, C] = Reconstruction(
    actualResolution,
    actualUniqueAnalysis,
    concretize
  )

  // ***************************
  //  Actual main program.
  // ***************************

  def execute(): Unit = {

    println()

    if (allFiles.isEmpty)
      println(s"No files found matching the given pattern ${config.input()}.")

    allFiles.foreach { f: File =>
      println(s"ANALYZING ${f.getPath}")
      println("***********************************************")

      println("Parsing...")
      val program = parse(f).get

      Statistics.clear()
      Statistics.inspectProgram(f.getAbsolutePath, program)

      for {
        cfg <- reconstruct(program)
      } {
        println("Writing to file...")
        write(cfg, f)

        Statistics.inspectCFG(cfg)
        if (config.stat.isDefined)
          Statistics.write(config.stat())
      }

      println("***********************************************")
      println()
    }

    println("Job complete. Exiting.")
    pool.shutdownNow()
  }


  import java.util.concurrent.{ExecutorService, Executors}

  private val pool: ExecutorService = Executors.newSingleThreadExecutor()
  private val factory = Executors.defaultThreadFactory()

  private def reconstruct(program: Program[I]): Option[ControlFlowGraph[I, C]] = {

    import scala.util.control.NonFatal

    println("Starting reconstruction...")

    var cfg: Option[ControlFlowGraph[I, C]] = None

    val thread = factory.newThread(() => {
      try {
        Statistics.startMeasure("total")
        cfg = Some(reconstruction.reconstruct(program).get)
        Statistics.stopMeasure("total")
      } catch {
        case _: ThreadDeath =>
          println("Reconstruction timed out.")
      }
    })

    try {
      thread.start()
      thread.join(config.timeout().toMillis, (config.timeout().toNanos - config.timeout().toMillis * 1000 * 1000).toInt)
    } catch {
      case NonFatal(e) =>
        println("An error occurred during reconstruction.")
        e.printStackTrace()
    }
    finally {
      thread.stop()
    }

    cfg
  }


  private def write(cfg: ControlFlowGraph[I, C], assemblyFile: File): Unit = {

    import au.edu.mq.comp.assembly.controlflow.DotConverter

    import org.bitbucket.franck44.dot.DOTPrettyPrinter.show

    import java.io.PrintWriter

    val file = new File(assemblyFile.getAbsolutePath + ".dot")
    val dot = DotConverter.toDot(cfg)

    val writer = new PrintWriter(file)
    writer.print(show(dot))
    writer.flush()
    writer.close()
  }
}
