package au.edu.mq.comp.assembly
package controlflow


object DotConverter {

  import org.bitbucket.franck44.dot.DOTSyntax._

  def toDot[I: InstructionSet](cfg: ControlFlowGraph[_, I]): DotSpec = {

    val nodeStyle = NodeStyle(ListAttributes(List(
      Attribute("shape", StringLit("rect")),
      Attribute("style", StringLit("filled,rounded")),
      Attribute("fillcolor", StringLit("#ff8e43")),
      Attribute("color", StringLit("transparent")),
      Attribute("fontname", StringLit(":bold"))
    )))
    val edgeStyle = EdgeStyle(ListAttributes(List(
      Attribute("style", StringLit("bold"))
    )))

    val nodeMap = cfg.states
      .zipWithIndex.toMap
      .mapValues(i => s"node$i")

    val nodeDecls = cfg.states
      .map(s => NodeDecl(Node(nodeMap(s)), ListAttributes(List(Attribute("label", StringLit(f"${cfg.location(s)}%04X"))))))

    val edges = cfg.states.flatMap { src =>
      cfg.successors(src).map { case (instruction, tgt) =>
        SingleTgtEdge(Node(nodeMap(src)), Node(nodeMap(tgt)), Some(ListAttributes(List(
          Attribute("label", StringLit(instruction.print))
        ))))
      }
    }

    val (initNodes, initEdges) = cfg.initialStates
      .map(createInitialNode[cfg.State](nodeMap))
      .unzip

    DotSpec(AutomatonName("cfg"), AutomatonBody(
      nodeStyle ::
        edgeStyle ::
        initNodes.toList :::
        nodeDecls.toList :::
        initEdges.toList :::
        edges.toList
    ))
  }

  private def createInitialNode[S](nodeNames: Map[S, String])(s: S): (NodeDecl, SingleTgtEdge) = {
    val name = nodeNames(s)
    val initName = Node(s"__init$name")

    val node = NodeDecl(
      initName,
      ListAttributes(List(
        Attribute("style", StringLit("invis")),
        Attribute("label", StringLit("")),
        Attribute("width", StringLit("0")),
        Attribute("height", StringLit("0"))
      ))
    )

    val edge = SingleTgtEdge(initName, Node(name), None)

    (node, edge)
  }
}
