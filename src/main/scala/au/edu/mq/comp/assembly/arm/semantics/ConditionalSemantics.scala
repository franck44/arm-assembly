package au.edu.mq.comp.assembly
package arm.semantics

import au.edu.mq.comp.assembly.arm.ConditionFlag._
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax._
import au.edu.mq.comp.assembly.semantics._
import au.edu.mq.comp.assembly.smt.Syntax._

/**
  * Augments a given semantics definition with conditional execution.
  *
  * @param underlyingSemantics The semantics definition to augment
  * @param nopSemantics        The fallback for conditional instructions, should their condition not be fulfilled
  */
case class ConditionalSemantics(underlyingSemantics: SemanticsDefinition[Instruction], nopSemantics: Semantics)
  extends SemanticsDefinition[Instruction] {

  def apply(instruction: Instruction): Semantics = {
    val unconditional = underlyingSemantics(instruction)

    instruction match {
      case ConditionalInstruction(cond) if cond != AL() =>
        val conditions = ConditionalInstruction.toStatePredicates(cond)
        new Semantics {
          override val isDefined: Set[StatePredicate] = for (a <- conditions ++ unconditional.isDefined; b <- nopSemantics.isDefined) yield a || b
          override val update: StateUpdate = conditions.conjunction.ite(unconditional.update, nopSemantics.update)
        }

      case _ => unconditional
    }
  }
}

object ConditionalInstruction {

  /**
    * Custom pattern matching to extract condition from instruction.
    */
  def unapply(instruction: Instruction): Option[Cond] = instruction match {
    case AccumulateInstruction(AccumulateMnemonic(_, _, cond), _, _, _, _) => Some(cond)
    case BatchMemoryInstruction(BatchMemoryMnemonic(_, _, cond), _, _, _)  => Some(cond)
    case CountBitInstruction(CountBitMnemonic(_, cond), _, _)              => Some(cond)
    case ComparisonInstruction(ComparisonMnemonic(_, cond), _, _)          => Some(cond)
    case DataInstruction(DataMnemonic(_, _, cond), _, _, _)                => Some(cond)
    case DynamicJumpInstruction(DynamicJumpMnemonic(_, cond), _, _)        => Some(cond)
    case LongInstruction(LongMnemonic(_, _, cond), _, _, _, _)             => Some(cond)
    case MemoryInstruction(MemoryMnemonic(_, _, cond), _, _)               => Some(cond)
    case MoveInstruction(MoveMnemonic(_, _, cond), _, _)                   => Some(cond)
    case NopInstruction(NopMnemonic(cond))                                 => Some(cond)
    case RotateInstruction(RotateMnemonic(_, _, cond), _, _)               => Some(cond)
    case StackInstruction(StackMnemonic(_, cond), _)                       => Some(cond)
    case StaticJumpInstruction(B(cond), _, _)                              => Some(cond)
    case StaticJumpInstruction(BL(cond), _, _)                             => Some(cond)
    case StaticJumpInstruction(BLX(cond), _, _)                            => Some(cond)
    case InstrWord(_)                                                      => None
  }

  /**
    * Converts a condition code to the state predicate it represents.
    */
  def toStatePredicates(cond: Cond): Set[StatePredicate] = cond match {
    case EQ() => Set(Z)
    case NE() => Set(!Z)
    case CS() => Set(C)
    case CC() => Set(!C)
    case MI() => Set(N)
    case PL() => Set(!N)
    case VS() => Set(V)
    case VC() => Set(!V)
    case HI() => Set(C, !Z)
    case LS() => Set(!C | Z)
    case GE() => Set(N === V)
    case LT() => Set(N =/= V)
    case GT() => Set(!Z, N === V)
    case LE() => Set(Z | (N =/= V))
    case AL() => Set.empty
  }

  /**
    * Concretizes a conditional instruction into guarded, unconditionally executed instructions;
    * explicitly specifying whether the condition was fulfilled or not.
    */
  def concretize(instruction: Instruction): Set[Guarded[UnconditionalInstruction]] = instruction match {
    case ConditionalInstruction(cond) if cond != AL() =>
      val predicates = toStatePredicates(cond)
      Set(
        Guarded(predicates, UnconditionalInstruction(instruction)),
        Guarded(Set(!predicates.conjunction), UnconditionalInstruction(NopInstruction(NopMnemonic(AL()))))
      )

    case _ => Set(Guarded(Set.empty, UnconditionalInstruction(instruction)))
  }
}

/**
  * Provides a wrapper around an instruction which indicates it is executed unconditionally.
  *
  * @param instruction The underlying unconditional instruction.
  */
final case class UnconditionalInstruction private(instruction: Instruction)

object UnconditionalInstruction {
  /**
    * Wraps the given instruction, setting its condition code to "always" (AL).
    */
  def apply(instruction: Instruction) = new UnconditionalInstruction(
    instruction = instruction match {
      case AccumulateInstruction(AccumulateMnemonic(o, s, _), r1, r2, r3, r4) => AccumulateInstruction(AccumulateMnemonic(o, s, AL()), r1, r2, r3, r4)
      case BatchMemoryInstruction(BatchMemoryMnemonic(o, a, _), r, u, l)      => BatchMemoryInstruction(BatchMemoryMnemonic(o, a, AL()), r, u, l)
      case ComparisonInstruction(ComparisonMnemonic(o, _), r, f)              => ComparisonInstruction(ComparisonMnemonic(o, AL()), r, f)
      case CountBitInstruction(CountBitMnemonic(o, _), r1, r2)                => CountBitInstruction(CountBitMnemonic(o, AL()), r1, r2)
      case DataInstruction(DataMnemonic(o, s, _), r1, r2, f)                  => DataInstruction(DataMnemonic(o, s, AL()), r1, r2, f)
      case DynamicJumpInstruction(DynamicJumpMnemonic(o, _), t, l)            => DynamicJumpInstruction(DynamicJumpMnemonic(o, AL()), t, l)
      case MemoryInstruction(MemoryMnemonic(o, s, _), r, m)                   => MemoryInstruction(MemoryMnemonic(o, s, AL()), r, m)
      case LongInstruction(LongMnemonic(o, s, _), r1, r2, r3, r4)             => LongInstruction(LongMnemonic(o, s, AL()), r1, r2, r3, r4)
      case MoveInstruction(MoveMnemonic(o, s, _), r, f)                       => MoveInstruction(MoveMnemonic(o, s, AL()), r, f)
      case NopInstruction(NopMnemonic(_))                                     => NopInstruction(NopMnemonic(AL()))
      case RotateInstruction(RotateMnemonic(o, s, _), r1, r2)                 => RotateInstruction(RotateMnemonic(o, s, AL()), r1, r2)
      case StackInstruction(StackMnemonic(o, _), l)                           => StackInstruction(StackMnemonic(o, AL()), l)
      case StaticJumpInstruction(B(_), t, l)                                  => StaticJumpInstruction(B(AL()), t, l)
      case StaticJumpInstruction(BL(_), t, l)                                 => StaticJumpInstruction(BL(AL()), t, l)
      case StaticJumpInstruction(BLX(_), t, l)                                => StaticJumpInstruction(BLX(AL()), t, l)
      case InstrWord(w)                                                       => InstrWord(w)
    }
  )
}