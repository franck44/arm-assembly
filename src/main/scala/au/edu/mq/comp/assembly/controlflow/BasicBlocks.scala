package au.edu.mq.comp.assembly.controlflow

/**
  * Represents a basic block in a control flow graph.
  * The instructions in a basic block are always executed in sequence.
  * Each node in a control flow graph is part of exactly one basic block.
  *
  * @param head The first CFG node belonging to the basic block.
  * @param tail The remainder of the basic block, as pairs of instruction and target node.
  * @tparam V The type of CFG nodes.
  * @tparam I The type of instructions.
  */
final case class BasicBlock[V, I](head: V, tail: List[(I, V)]) {
  lazy val nodes: List[V] = head :: tail.map(_._2)

  lazy val instructions: List[I] = tail.map(_._1)
}

/**
  * Represents a CFG in basic block-structure.
  *
  * @param blocks      The CFG's basic blocks.
  * @param successors  A map associating blocks and instructions with the corresponding target blocks.
  * @param entryBlocks The set of initial blocks.
  * @tparam V The type of CFG states of the original CFG.
  * @tparam I The type of instructions.
  */
final case class BasicBlocks[V, I](
                                    blocks: Set[BasicBlock[V, I]],
                                    successors: Map[(BasicBlock[V, I], I), Set[BasicBlock[V, I]]],
                                    entryBlocks: Set[BasicBlock[V, I]]
                                  ) {
  require(entryBlocks.subsetOf(blocks), "Entry blocks must be contained in the set of blocks.")

  lazy val predecessors: Map[BasicBlock[V, I], Set[(I, BasicBlock[V, I])]] = {
    val pre = for {
      ((pre, iota), succs) <- successors.toSet
      succ <- succs
    } yield (succ, (iota, pre))
    pre.groupBy(_._1).mapValues(_.map(_._2))
  }

  lazy val blockOf: Map[I, (BasicBlock[V, I], Int)] = (
    for {
      block <- blocks
      ((instruction, _), i) <- block.tail.zipWithIndex
    } yield instruction -> (block, i)
    ).toMap
}

object BasicBlocks {
  /**
    * Converts a CFG to basic block-structure.
    *
    * @param cfg The CFG to convert.
    * @tparam I The type of (concrete) instructions in the CFG.
    * @return An equivalent CFG in basic block-structure.
    */
  def apply[I](cfg: ControlFlowGraph[_, I]): BasicBlocks[cfg.State, I] = {
    val initialNodes = cfg.states.filter(isInitial(cfg))

    // Compute predecessor map once for fast lookup; thread through computation unmodified.
    val predecessors: Map[cfg.State, Set[cfg.State]] =
      cfg.states
        .flatMap(v => cfg.successors(v).map { case (_, w) => (w, v) })
        .groupBy(_._1)
        .mapValues(_.map(_._2))

    val (heads, edges) = computeBlocks(cfg)(initialNodes, Set.empty, predecessors)

    BasicBlocks(
      heads.values.toSet,
      edges.mapValues(_.map(heads)),
      initialNodes.map(heads)
    )
  }

  /**
    * Computes the basic blocks of the given CFG recursively.
    *
    * @param cfg          The CFG to convert to basic block-structure.
    * @param workList     A set of nodes that are heads of basic blocks yet to be created.
    * @param done         A set of nodes that are heads of basic blocks already created.
    * @param predecessors A map from nodes to their predecessors in the original CFG.
    * @tparam I The type of (concrete) instructions in the CFG.
    * @return A tuple containing two maps. The first maps CFG nodes to basic blocks of which they are the head node. The second maps basic blocks and concrete instructions to the head nodes of the corresponding successor blocks.
    */
  private def computeBlocks[I](cfg: ControlFlowGraph[_, I])(workList: Set[cfg.State], done: Set[cfg.State], predecessors: Map[cfg.State, Set[cfg.State]]): (Map[cfg.State, BasicBlock[cfg.State, I]], Map[(BasicBlock[cfg.State, I], I), Set[cfg.State]]) = {
    workList.toList match {
      case v :: remainder =>
        val (tail, last) = collectBlock(cfg)(v, predecessors)
        val block = BasicBlock(v, tail)

        val newBlockHeads = cfg.successors(last).collect { case (_, w) if w != v && !done.contains(w) => w }
        val newEdges = cfg.successors(last).groupBy(_._1).map { case (iota, s) => (block, iota) -> s.map(_._2) }

        val (heads, edges) = computeBlocks(cfg)(newBlockHeads ++ remainder, done + v, predecessors)
        (heads + (v -> block), edges ++ newEdges)
      case Nil            =>
        (Map.empty, Map.empty)
    }
  }

  /**
    * Collects the tail of a basic block starting at the given node (recursively).
    *
    * @param cfg          The CFG in which the basic block is located.
    * @param w            The head node of the basic block.
    * @param predecessors A map from nodes to their predecessors in the original CFG.
    * @tparam I The type of (concrete) instructions in the CFG.
    * @return The tail of the basic block (may be empty), and the last node in the basic block.
    */
  private def collectBlock[I](cfg: ControlFlowGraph[_, I])(w: cfg.State, predecessors: Map[cfg.State, Set[cfg.State]]): (List[(I, cfg.State)], cfg.State) = {
    cfg.successors(w).toList match {
      case List((iota, w2)) if predecessors(w2).size == 1 && !isInitial(cfg)(predecessors(w2).head) =>
        val (block, last) = collectBlock(cfg)(w2, predecessors)
        ((iota, w2) :: block, last)

      case _ =>
        (Nil, w)
    }
  }

  private def isInitial(cfg: ControlFlowGraph[_, _])(v: cfg.State): Boolean = cfg.location(v) == cfg.program.initialLocation // TODO
}
