package au.edu.mq.comp.assembly.arm.syntax

import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax._

object Printer extends ARMPrettyPrinter {
  override def value(v: Any): Doc = v match {
    case i: BigInt => f"$i%04X"
    case _         => super.value(v)
  }

  override def toDoc(astNode: ARMSyntax.ASTNode): Doc = astNode match {
    case R15() => text("pc")
    case R14() => text("lr")
    case R13() => text("sp")
    case R12() => text("ip")
    case R11() => text("fp")
    case R10() => text("sl")

    case CS() => text("cs")
    case CC() => text("cc")

    // Prefix constants with # and print them in decimal.
    case ConstantOperand(const) => char('#') <> const.toString

    case _ => super.toDoc(astNode)
  }
}
