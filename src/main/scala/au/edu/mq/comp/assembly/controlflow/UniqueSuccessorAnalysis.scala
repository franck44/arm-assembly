package au.edu.mq.comp.assembly.controlflow

import au.edu.mq.comp.assembly.Location

/**
  * Represents a (possibly instruction set-specific) analysis of instructions and their unique successors, if they exist.
  *
  * @tparam I The type of analyzed instructions.
  */
trait UniqueSuccessorAnalysis[I] {
  /**
    * Checks if an instruction has degree <= 1.
    *
    * @param instruction The instruction to analyze.
    * @return `Some(true)` if the degree is known to be no greater than 1, `Some(false)` if it is greater than 1, or None if the answer could not be determined.
    */
  @deprecated
  def hasUniqueSuccessor(instruction: I): Option[Boolean]

  /**
    * Computes an instruction's unique successor, if it exists.
    *
    * @param location    The instruction's location.
    * @param instruction The instruction itself.
    * @return A result containing the successor location (or none if there is none), if it could be computed.
    */
  def computeUniqueSuccessor(location: Location, instruction: I): UniqueSuccessorAnalysis.Result
}

object UniqueSuccessorAnalysis {

  sealed trait Result

  object NoSuccessors extends Result

  final case class SingleSuccessor(location: Location) extends Result

  object Unknown extends Result

  case class Dummy[I]() extends UniqueSuccessorAnalysis[I] {
    override def hasUniqueSuccessor(instruction: I): Option[Boolean] = None

    override def computeUniqueSuccessor(location: Location, instruction: I): Result = Unknown
  }

}
