package au.edu.mq.comp.assembly
package arm.semantics

import au.edu.mq.comp.assembly.arm.ArmProcessor._
import au.edu.mq.comp.assembly.arm.semantics.IntegerSupport._
import au.edu.mq.comp.assembly.arm.syntax.PC
import au.edu.mq.comp.assembly.semantics._
import au.edu.mq.comp.assembly.smt.Syntax._

import org.bitbucket.franck44.scalasmt.theories.{ArrayTerm, BVTerm}

/**
  * Library of shared functions used by instruction semantics, expressed in scalaSMT terms.
  * Translated from pseudo-code in ARM specification.
  */
object Library {
  // Taken from the ARM Architecture Reference Manual (ARMv8, for ARMv8-A architecture profile) version B.a.
  // <https://static.docs.arm.com/ddi0487/b/DDI0487B_a_armv8_arm.pdf>
  // Page numbers in comments refer to this document.

  /**
    * MemU - non-assignment form (p. 5970).
    *
    * @param address The address to read from.
    * @param size    The number of bytes to read.
    * @return data in the form of an (8 * size) bits long bit vector.
    */
  def readMemU(address: BV, size: Int): Guarded[BV] = readMemWithType(address, size, "normal")

  /**
    * MemU - assignment form (p. 5970).
    *
    * @param address The address to which a value is written.
    * @param size    The number of bytes to write.
    * @param value   The value to be written. Must be (8 * size) bits long.
    * @param N       The size of the address bit vector.
    */
  def writeMemU(address: BV, size: Int, value: BV)(implicit N: WordSize): Guarded[StateUpdate] = writeMemWithType(address, size, "normal", value)


  /**
    * MemA - non-assignment form (p. 5970)
    *
    * @param address The address from which data is read.
    * @param size    Then number of bytes to read.
    * @return data in the form of an (8 * size) bits long bit vector.
    */
  def readMemA(address: BV, size: Int): Guarded[BV] = readMemWithType(address, size, "atomic")

  /**
    * MemA - assignment form (p. 5970)
    *
    * @param address The address to which a value is written
    * @param size    The number of bytes to write.
    * @param value   The value to be written. Must be (8 * size) bits long.
    * @param N       The size of the address bit vector.
    */
  def writeMemA(address: BV, size: Int, value: BV)(implicit N: WordSize): Guarded[StateUpdate] = writeMemWithType(address, size, "atomic", value)

  /**
    * Mem_with_type - non-assignment form (p. 5971).
    *
    * @param address    The address from which data is read.
    * @param size       The number of bytes to read.
    * @param accessType The access type, currently unused.
    */
  def readMemWithType(address: BV, size: Int, accessType: Any): Guarded[BV] = {
    assert(Set(1, 2, 4, 8, 16).contains(size)) {
      val iswrite = false

      val aligned = true //*TODO: check alignment
      val value = if (!aligned)
        ???
      else {
        // TODO: how much detail for the semantics of memory access? This is a simplification of the original pseudo-code.
        val bytes = (0 until size).map(offset => memory.at(address + offset.ui32))
        bytes.reverse.reduceLeft[BV](_ concat _)
      }

      if (false) // TODO: BigEndian()
        ???
      else
        value
    }
  }

  /**
    * Mem_with_type - assignment form (p. 5971).
    *
    * @param address    The address to which data is written.
    * @param size       The number of bytes to be written-
    * @param accessType The access type, currently unused.
    * @param valueIn    The value to be written. Must be (8 * size) bits long.
    * @param N          The size of the address bit vector.
    */
  def writeMemWithType(address: BV, size: Int, accessType: Any, valueIn: BV)(implicit N: WordSize): Guarded[StateUpdate] = {
    val iswrite = true
    val value = if (false) // TODO: BigEndian()
      ???
    else valueIn

    val aligned = true // TODO: check alignment
    if (!aligned)
      ???
    else {
      // TODO: how much detail for the semantics of memory access? This is a simplification of the original pseudo-code.
      val updated = (0 until size).foldLeft(memory: Expr[ArrayTerm[BVTerm]]) { case (mem, offset) => mem(address + offset.bvu) := value.extract(8 * offset + 7, 8 * offset) }
      memory |-> updated
    }
  }

  /**
    * Returns the PC value.
    */
  def pcStoreValue(): BV = PC().asVar

  /**
    * Write an address loaded from memory to the PC (p. 5976).
    *
    * @param address The loaded address.
    */
  def loadWritePC(address: BV): Guarded[StateUpdate] = bxWritePC(address)

  /**
    * Write the PC from a branch (p. 5975).
    *
    * @param address The address.
    */
  def branchWritePC(address: BV)(implicit N: WordSize): StateUpdate = {
    val aligned = address and !0x3.bvu // clear the last two bits
    branchTo(aligned, "unknown")
  }

  /**
    * Write an address to the PC (p. 5975).
    *
    * @param address The address.
    */
  def bxWritePC(address: BV): Guarded[StateUpdate] = {
    assert(isAligned(address)) {
      branchTo(address, "unknown")
    }
  }

  /**
    * Write an address to the PC (p. 5975).
    *
    * @param address The address.
    */
  def aluWritePC(address: BV): Guarded[StateUpdate] = bxWritePC(address)

  /**
    * Set the program counter to a new address (p. 6070).
    * Adds the PC offset to the value to account for the fetch/decode stages.
    *
    * @param target      The target address.
    * @param branch_type A branch type. Currently unused.
    */
  def branchTo(target: BV, branch_type: Any): StateUpdate = PC().asVar |-> (target + pcOffset.ui32)


  /**
    * Checks that the given address is aligned to a word boundary.
    * Helper function, not taken from pseudo-code.
    *
    * @param address The address to check.
    */
  def isAligned(address: BigInt): Boolean = address % 4 == 0

  /**
    * Checks that the given address is aligned to a word boundary.
    * Helper function, not taken from pseudo-code.
    *
    * @param address The address to check.
    */
  def isAligned(address: BV): Formula = address.extract(1, 0) === 0.withUBits(2)

  /**
    * Align() (p. 6034)
    */
  def align(x: BV, y: Int)(implicit N: WordSize): BV =
    if (y == 4) // common case optimization
      x.extract(N - 1, 2) concat 0.withUBits(2)
    else
      align(UInt(x), y).extract(N - 1, 0)

  /**
    * Align() (p. 6034)
    */
  def align(x: Integer, y: Integer): Integer = y * (x / y)

  /** Adds two bit vectors, plus an optional carry bit. Returns result and NZCV flags. */
  def addWithCarry(x: BV, y: BV, carryIn: Formula)(implicit N: WordSize): (BV, Formula, Formula, Formula, Formula) = {
    val unsigned_sum = UInt(x) + UInt(y) + UInt(carryIn.bit)(WordSize.Bit)
    val signed_sum = SInt(x) + SInt(y) + SInt(carryIn.bit)(WordSize.Bit)
    val result = unsigned_sum.bv.extract(N - 1, 0)

    val n = result(N - 1)
    val z = isZero(result)
    val c = UInt(result) =/= unsigned_sum
    val v = SInt(result) =/= signed_sum

    (result, n, z, c, v)
  }

  def shift(value: BV, typ: SRType, amount: Integer, carryIn: Formula)(implicit wordSize: WordSize): BV = {
    val (result, _) = shiftC(value, typ, amount, carryIn)
    result
  }

  def shiftC(value: BV, typ: SRType, amount: Integer, carryIn: Formula)(implicit wordSize: WordSize): (BV, Formula) = {
    if (amount.literalValue.contains(0)) // small optimization: do nothing for literal 0
      (value, carryIn)
    else {
      // If amount is 0, all these guarantee that carryOut === false.
      val (result, carryOut) = typ match {
        case SRType.LSL => lslC(value, amount)
        case SRType.LSR => lsrC(value, amount)
        case SRType.ASR => asrC(value, amount)
        case SRType.ROR => rorC(value, amount)
        case SRType.RRX => rrxC(value, carryIn) // assumes amount === 1
      }

      // equivalent to (amount === 0).ite(carryIn, carryOut), given that carryOut => (amount =/= 0).
      (result, (amount === 0 && carryIn) || carryOut)
    }
  }

  def lsl(x: BV, shift: Int)(implicit N: WordSize): BV = {
    if (shift == 0)
      x
    else {
      val (result, _) = lslC(x, shift)
      result
    }
  }

  // Note: this implementation can handle a shift of 0.
  // However, in that case the carry value will always be false.
  def lslC(x: BV, shift: Integer)(implicit N: WordSize): (BV, Formula) = {
    val result = x << shift.asBitVector(N)
    val extended = (0.withUBits(1) concat x) << shift.asBitVector(N + 1)
    val carryOut = extended(N)
    (result, carryOut)
  }

  def lsr(x: BV, shift: Int)(implicit N: WordSize): BV = {
    if (shift == 0)
      x
    else {
      val (result, _) = lsrC(x, shift)
      result
    }
  }

  // Note: this implementation can handle a shift of 0.
  // However, in that case the carry value will always be false.
  def lsrC(x: BV, shift: Integer)(implicit N: WordSize): (BV, Formula) = {
    val result = x >> shift.asBitVector(N)
    val extended = (x concat 0.withUBits(1)) >> shift.asBitVector(N + 1)
    val carryOut = extended(0)
    (result, carryOut)
  }

  // Note: this implementation can handle a shift of 0.
  // However, in that case the carry value will always be false.
  def asrC(x: BV, shift: Integer)(implicit N: WordSize): (BV, Formula) = {
    val result = x ashr shift.asBitVector(N)
    val extended = (x concat 0.withUBits(1)) ashr shift.asBitVector(N + 1)
    val carryOut = extended(0)
    (result, carryOut)
  }

  // Note: this implementation can handle a shift of 0.
  // However, in that case the carry value will always be false.
  def rorC(x: BV, shift: Integer)(implicit N: WordSize): (BV, Formula) = {
    val m = shift % N.size
    val result = (x >> m.asBitVector(N)) or (x << (N.size - m).asBitVector(N))
    val carryOut = result(N - 1)
    (result, shift =/= 0 && carryOut)
  }

  def rrxC(x: BV, carryIn: Formula)(implicit N: WordSize): (BV, Formula) = {
    val result = carryIn.bit concat x.extract(N - 1, 1)
    val carryOut = x(0)
    (result, carryOut)
  }

  sealed trait SRType

  object SRType {

    case object LSL extends SRType

    case object LSR extends SRType

    case object ASR extends SRType

    case object ROR extends SRType

    case object RRX extends SRType

  }

  /** Compares a bit vector to 0. */
  def isZero(x: BV)(implicit N: WordSize): Formula = x === 0.bvu

  // Bitwise Magic

  // p. 6034
  def countLeadingZeroBits(x: BV)(implicit N: WordSize): Integer = N - 1 - highestSetBit(x)

  def highestSetBit(x: BV)(implicit N: WordSize): Integer = {
    val log = math.log(N.size).toInt
    val y = (0 until log).foldLeft(x) { case (z, i) => z or (z >> (1 << i).bvu) }
    countOnes(y) - 1
  }

  // <https://en.wikipedia.org/wiki/Hamming_weight>
  def countOnes(x: BV)(implicit N: WordSize): Integer = {
    val log = math.log(N.size).toInt
    val result = (0 until log).foldLeft(x) { case (z, i) =>
      val p = 1 << i // power of 2
      val m = BigInt((("0" * p) + ("1" * p)) * (N / (2 * p)), 2).bvu // bitvector of p zeros, p ones, p zeros, ...
      (z and m) + ((z >> p.bvu) and m)
    }
    UInt(result)
  }
}
