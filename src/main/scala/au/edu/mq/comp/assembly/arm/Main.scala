package au.edu.mq.comp.assembly
package arm

import au.edu.mq.comp.assembly.arm.semantics.UnconditionalInstruction


object Main {

  private implicit val uis: InstructionSet[UnconditionalInstruction] = AArch32.Unconditional

  def main(args: Array[String]): Unit = {
    new ArmApplication(args).execute()
  }
}


import au.edu.mq.comp.assembly.application.ApplicationBase
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax.Instruction

class ArmApplication(args: Array[String])(implicit uis: InstructionSet[UnconditionalInstruction])
  extends ApplicationBase[Instruction, Guarded[UnconditionalInstruction]](args) {

  import au.edu.mq.comp.assembly.arm.controlflow.SuccessorAnalysis
  import au.edu.mq.comp.assembly.arm.semantics.ConditionalInstruction
  import au.edu.mq.comp.assembly.controlflow.UniqueSuccessorAnalysis

  import scala.util.Try

  import java.io.File

  override protected lazy val concretize: Instruction => Set[Guarded[UnconditionalInstruction]] =
    ConditionalInstruction.concretize

  override protected lazy val uniqueAnalysis: UniqueSuccessorAnalysis[Guarded[UnconditionalInstruction]] =
    SuccessorAnalysis(config.simplifierUnique())(implicitly, buildContext(config.smtUnique(), config.smtmodeUnique()))

  override protected def parse(file: File): Try[Program[Instruction]] = AArch32.buildProgram(file)
}