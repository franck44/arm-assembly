package au.edu.mq.comp.assembly
package smt

import au.edu.mq.comp.assembly.smt.Syntax.AnyQId

import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
import org.bitbucket.franck44.scalasmt.typedterms.TypedTerm
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter.{reduce, rewrite, rule}

object Z3Normalizer {
  /**
    * Normalizes an expression using Z3-only syntax constructs, replacing them with equivalent scalaSMT standard syntax.
    */
  def normalize(expr: Term): Term =
    rewrite(reduce(normalization))(expr)

  def normalize[T](expr: Expr[T]): Expr[T] = new TypedTerm[T, Term](expr.typeDefs, normalize(expr.termDef))

  private lazy val normalization = eliminateNAry + eliminateCustomDivisions + eliminateBit2Bool

  // Z3 has n-ary operations in some cases where SMT-LIB / scalaSMT only has a binary operator.
  // Eliminate by repeatedly applying binary operator (left-associative).
  private lazy val eliminateNAry = rule[Term] {
    case QIdAndTermsTerm(AnyQId(SymbolId(SSymbol("concat"))), terms) => terms.reduceLeft(BVConcatTerm) // not in SMT-LIB
    case QIdAndTermsTerm(AnyQId(SymbolId(SSymbol("bvadd"))), terms)  => terms.reduceLeft(BVAddTerm)    // SMT-LIB but not scalaSMT
    case QIdAndTermsTerm(AnyQId(SymbolId(SSymbol("bvxor"))), terms)  => terms.reduceLeft(BVXorTerm)    // SMT-LIB but not scalaSMT
  }

  // Z3 has an indexed bit2bool operation that extracts a bit (given by the index) and compares it to 1.
  // Eliminate by doing this explicitly.
  private lazy val eliminateBit2Bool = rule[Term] {
    case QIdAndTermsTerm(AnyQId(IndexedId(SSymbol("bit2bool"), List(NumeralIdx(num)))), List(term)) => EqualTerm(
      BVExtractTerm(NumLit(num), NumLit(num), term),
      ConstantTerm(DecBVLit(BVvalue("1"), "1"))
    )
  }

  // Z3 has custom division operations that have the same semantics as the original,
  // but are created internally when the second operand is known to be non-zero.
  // <https://github.com/Z3Prover/z3/issues/1133>
  private lazy val eliminateCustomDivisions = rule[Term] {
    case QIdAndTermsTerm(AnyQId(SymbolId(SSymbol("bvsdiv_i"))), List(left, right)) => BVsDivTerm(left, right)
    case QIdAndTermsTerm(AnyQId(SymbolId(SSymbol("bvudiv_i"))), List(left, right)) => BVuDivTerm(left, right)
    case QIdAndTermsTerm(AnyQId(SymbolId(SSymbol("bvsrem_i"))), List(left, right)) => BVsRemTerm(left, right)
    case QIdAndTermsTerm(AnyQId(SymbolId(SSymbol("bvurem_i"))), List(left, right)) => BVuRemTerm(left, right)

    // bvsmod apparently unsupported by scalaSMT
    //case QIdAndTermsTerm(AnyQId(SymbolId(SSymbol("bvsmod_i"))), List(left, right))    => BVsModTerm(left, right)
  }

}
