package au.edu.mq.comp.assembly.arm.syntax

object Helper {
  def parseConstant(s: String): Int = {
    val literal = s.stripMargin('#') // TODO: # should not be present (if possible)
    val (radix, data) = if (literal.startsWith("0x"))
      (16, literal.substring(2))
    else if (literal.startsWith("0b"))
      (2, literal.substring(2))
    else
      (10, literal)
    Integer.parseInt(data, radix)
  }

  def parseBigConstant(s: String): BigInt = {
    val literal = s.stripMargin('#') // TODO: # should not be present (if possible)
    val (radix, data) = if (literal.startsWith("0x"))
      (16, literal.substring(2))
    else if (literal.startsWith("0b"))
      (2, literal.substring(2))
    else
      (10, literal)
    BigInt(data, radix)
  }

  def parseHexConstant(s: String): BigInt = BigInt(s.stripMargin('#'), 16) // TODO: # should not be present (if possible)
}
