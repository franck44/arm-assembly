package au.edu.mq.comp.assembly

import au.edu.mq.comp.assembly.controlflow.{ControlFlowGraph, PredicateAutomatonResolver, Resolver}

import com.typesafe.scalalogging.LazyLogging
import org.bitbucket.franck44.automat.auto.{DetAuto, NFA}
import org.bitbucket.franck44.automat.util.Determiniser.toDetNFA
import org.bitbucket.franck44.automat.util.DotConverter.toDot
import org.bitbucket.franck44.dot.DOTPrettyPrinter
import org.bitbucket.franck44.dot.DOTSyntax.{Attribute, StringLit}
import org.bitbucket.franck44.scalasmt.parser.SMTLIB2PrettyPrinter
import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.Term

/**
  * Some common logging functionality.
  */
private[assembly] trait Logging extends LazyLogging {
  protected def logInstructions[I: InstructionSet](message: String, instructions: Seq[I], level: LogLevel = LogLevel.Debug): Unit = {
    log(level, message)
    for (instruction <- instructions)
      log(level, "\t" + instruction.print)
  }

  protected def logFormulas(message: => String, formulas: => Seq[Formula], level: LogLevel = LogLevel.Debug): Unit = {
    log(level, message)
    for (formula <- formulas)
      log(level, s"\t${formula.show}")
  }

  protected def logCFG[I: InstructionSet](message: => String, cfg: => ControlFlowGraph[_, I], level: LogLevel = LogLevel.Debug): Unit = {
    log(level, message)
    log(level, {
      val graph = cfg
      val dot = toDot(
        graph.toLabeledNFA,
        nodeDotName  = quote,
        nodeProp     = (x: graph.State) => List(Attribute("label", StringLit(f"${graph.location(x)}%04X")), Attribute("tooltip", StringLit(x.toString))),
        labelDotName = (i: (Location, I)) => i.print
      )
      DOTPrettyPrinter.show(dot)
    })
  }

  protected def logTraceCFG[I: InstructionSet](message: => String, cfg: ControlFlowGraph[_, I], level: LogLevel = LogLevel.Debug)(states: Seq[cfg.State]): Unit = {
    log(level, message)
    log(level, {
      val map = states.zipWithIndex.groupBy(_._1).mapValues(_.map(_._2).toSet)
      val dot = toDot(
        cfg.toLabeledNFA,
        nodeDotName = quote,
        nodeProp = (x: cfg.State) => {
          val indices = map.getOrElse(x, Set.empty).toSeq.sorted.mkString(",")
          List(
            Attribute("label", StringLit(f"${cfg.location(x)}%04X $indices%s")),
            Attribute("tooltip", StringLit(x.toString)),
            Attribute("style", StringLit("filled")),
            Attribute("fillcolor", StringLit(if (indices == "") "gray" else "red"))
          )
        },
        labelDotName = (i: (Location, I)) => i.print
      )
      DOTPrettyPrinter.show(dot)
    })
  }

  protected def logDFA[S, L](message: => String, dfa: => DetAuto[S, L], level: LogLevel = LogLevel.Debug): Unit = {
    log(level, message)
    log(level, {
      val (nfa, names) = toDetNFA(dfa, quote)
      DOTPrettyPrinter.show(toDot(nfa, nodeDotName = names))
    })
  }

  protected def logNFA[S, L](message: => String, nfa: => NFA[S, L], level: LogLevel = LogLevel.Debug): Unit = {
    log(level, message)
    log(level, DOTPrettyPrinter.show(toDot(nfa, nodeDotName = quote)))
  }

  protected def logPredicateAutomaton[I: InstructionSet](message: => String, nfa: => NFA[Term, I], level: LogLevel = LogLevel.Debug): Unit = {
    log(level, message)
    log(level, DOTPrettyPrinter.show(toDot(nfa, nodeDotName = quote compose (SMTLIB2PrettyPrinter.show(_: Term)), labelDotName = { i: I => i.print })))
  }

  protected def logPredicateResolver[I: InstructionSet](message: => String, resolver: PredicateAutomatonResolver[I], level: LogLevel = LogLevel.Debug): Unit = {
    log(level, message)
    log(level, {
      DOTPrettyPrinter.show(toDot(
        resolver.nfa,
        nodeDotName = (p: Term) => quote(s"${SMTLIB2PrettyPrinter.show(p)}\\n${printLabel(resolver.labeling.get(p))}"),
        labelDotName = { i: I => i.print }
      ))
    })
  }

  private def printLabel(l: Option[Set[Location]]): String = l match {
    case Some(locations) => s"{ ${locations.map(_.toString(16)).mkString(", ")} }"
    case None            => "⊥"
  }

  protected def logResolver[I: InstructionSet](message: => String, resolver: => Resolver[I], level: LogLevel = LogLevel.Debug): Unit = {
    log(level, message)
    log(level, {
      val r = resolver
      val (nfa, stateLabels) = toDetNFA(r.dfa, { s: r.State => printLabel(r.label(s)) })
      DOTPrettyPrinter.show(toDot(nfa, nodeDotName = (i: Int) => quote(s"q$i: ${stateLabels(i)}"), labelDotName = { i: I => i.print }))
    })
  }

  private def quote[A]: A => String = (a: A) => '"' + a.toString + '"'

  protected def log(level: LogLevel, message: => String): Unit = {
    level match {
      case LogLevel.Debug => logger.debug(message)
      case LogLevel.Info  => logger.info(message)
      case LogLevel.Warn  => logger.warn(message)
      case LogLevel.Error => logger.error(message)
    }
  }

  protected sealed trait LogLevel

  protected object LogLevel {

    case object Debug extends LogLevel

    case object Info extends LogLevel

    case object Warn extends LogLevel

    case object Error extends LogLevel

  }

}
