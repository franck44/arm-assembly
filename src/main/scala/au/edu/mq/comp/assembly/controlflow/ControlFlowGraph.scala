package au.edu.mq.comp.assembly.controlflow

import au.edu.mq.comp.assembly.{Location, Program}

import org.bitbucket.franck44.automat.auto.NFA

trait ControlFlowGraph[I1, I2] {
  type State

  /** CFG members (abstract) **/

  def states: Set[State]

  def initialStates: Set[State]

  def location(state: State): Location

  def program: Program[I1]

  def successors(state: State): Set[(I2, State)]

  /** NFA conversion **/

  def toUnlabeledNFA: NFA[State, I2] = {
    import org.bitbucket.franck44.automat.edge.Implicits._

    val transitions = for {
      s <- states
      (i, t) <- successors(s)
    } yield (s ~> t) (i)

    NFA(initialStates, transitions, states, name = "CFG (no name)")
  }

  def toLabeledNFA: NFA[State, (Location, I2)] = {
    val unlabeledNFA = toUnlabeledNFA
    val labeledTransitions = unlabeledNFA.transitions.map { edge => edge.copy(lab = (location(edge.unLabelled.src), edge.lab)) }
    unlabeledNFA.copy(transitions = labeledTransitions)
  }

  /** Convenience accessors **/

  def instruction(state: State) = program.instructionAt(location(state))

  import org.bitbucket.franck44.automat.util.{DFSVisitor, Traversal}

  def dfs[V <: DFSVisitor[State, I2, V]](initialVisitor: V, initialState: State): V = Traversal.DFS(
    initialVisitor,
    { s: State => successors(s).toList }
  )(initialState)
}
