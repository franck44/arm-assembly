package au.edu.mq.comp.assembly

import au.edu.mq.comp.assembly.semantics.Semantics

import scala.util.Try

/**
  * Represents a set of instructions with an associated semantics.
  *
  * @tparam I The type of instructions.
  */
trait InstructionSet[I] {
  /**
    * The machine on which instructions are executed.
    */
  val machine: Machine

  /**
    * Parses an instruction from a string.
    */
  def parse(source: String): Try[I]

  /**
    * Pretty-prints an instruction.
    */
  def print(instruction: I): String

  /**
    * Gets the semantics associated with an instruction.
    */
  def semantics(instruction: I): Semantics
}

object InstructionSet {
  /**
    * Provides an implicit instruction set for labeled instructions.
    *
    * @tparam I The type of instructions.
    */
  implicit def controlFlowAwareInstructionSet[I: InstructionSet]: InstructionSet[(Location, I)] = ControlFlowAwareInstructionSet[I]()

  /**
    * Provides an implicit instruction set for guarded instructions.
    *
    * @tparam I The type of instructions.
    */
  implicit def guardedInstructionSet[I: InstructionSet]: InstructionSet[Guarded[I]] = GuardedInstructionSet[I]()
}


/**
  * An instruction set of arbitrary instructions labeled with their memory locations.
  *
  * @tparam I The type of instructions.
  */
case class ControlFlowAwareInstructionSet[I]()(implicit baseInstructionSet: InstructionSet[I]) extends InstructionSet[(Location, I)] {

  override val machine: Machine = baseInstructionSet.machine

  override def parse(source: String): Try[(Location, I)] = ???

  override def print(labeledInstruction: (Location, I)): String = {
    val (location, instruction) = labeledInstruction
    f"$location%04X: ${instruction.print}"
  }

  override def semantics(labeledInstruction: (Location, I)): Semantics = {

    import au.edu.mq.comp.assembly.semantics.StateUpdate
    import au.edu.mq.comp.assembly.smt.Syntax._

    val (location, instruction) = labeledInstruction
    val instructionSemantics = instruction.semantics
    machine.locationToExpression(location).map { pcValue =>
      new Semantics {
        override val isDefined: Set[StatePredicate] = instructionSemantics.isDefined + (machine.pcExpr === pcValue)
        override val update: StateUpdate = instructionSemantics.update
      }
    }.getOrElse(Semantics.undefined)
  }
}


/**
  * The canonical instruction set for guarded instructions.
  *
  * @tparam I The type of the (unguarded) instructions.
  */
case class GuardedInstructionSet[I]()(implicit instructionSet: InstructionSet[I]) extends InstructionSet[Guarded[I]] {

  import au.edu.mq.comp.assembly.smt.Syntax._

  import scala.util.Failure

  override val machine: Machine = instructionSet.machine

  override def parse(source: String): Try[Guarded[I]] = Failure(???)

  override def print(instruction: Guarded[I]): String = {
    val guard = instruction.guards.conjunction
    if (guard == True())
      instruction.value.print
    else
      s"${instruction.guards.conjunction.show} / ${instruction.value.print}"
  }

  override def semantics(guarded: Guarded[I]): Semantics = guarded match {
    case Guarded(guards, instruction) =>

      import au.edu.mq.comp.assembly.semantics.StateUpdate

      val instructionSemantics = instruction.semantics
      new Semantics {
        override val isDefined: Set[StatePredicate] = guards ++ instructionSemantics.isDefined
        override val update: StateUpdate = instructionSemantics.update
      }
  }
}