package au.edu.mq.comp.assembly
package semantics

import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
import org.bitbucket.franck44.scalasmt.theories.{ArrayTerm, BVTerm, BoolTerm}
import org.bitbucket.franck44.scalasmt.typedterms.VarTerm

object DataFlowAnalysis {
  /**
    * The set of program variables that influence whether or not an instruction can be executed in a state.
    */
  def checkedVariables[I: InstructionSet](instruction: I): TypedSet[ProgramVariable] =
    instruction.semantics
      .isDefined
      .flatMap(_.typeDefs)
      .map(sid => TypedSet(toVariable(sid).variable))
      .foldLeft(TypedSet.empty[ProgramVariable])(_ ++ _)

  /**
    * The set of program variables that may be modified by an instruction.
    */
  def definedVariables[I: InstructionSet](instruction: I): TypedSet[ProgramVariable] =
    instruction.semantics
      .update
      .filter(isModified)
      .keys

  /**
    * The set of program variables that may influence the new value of a program variable modified by an instruction.
    */
  def usedVariables[I: InstructionSet](instruction: I): Set[ProgramVariable[_]] =
    usedVariables(instruction, definedVariables(instruction).toSet)

  /**
    * The set of program variables that may influence the new value of a program variable in the given set after execution of the given instruction.
    */
  def usedVariables[I: InstructionSet](instruction: I, variables: Set[ProgramVariable[_]]): Set[ProgramVariable[_]] = {
    val update = instruction.semantics.update
    variables.map { case x: ProgramVariable[e] => update.getOrElse(x, x) }
      .flatMap(_.typeDefs)
      .map(toVariable(_).variable)
  }

  /**
    * The set of program variables that may influence the new value of the given program variable after execution of the given instruction.
    */
  def usedVariables[I: InstructionSet](instruction: I, variable: ProgramVariable[_]): Set[ProgramVariable[_]] =
    usedVariables(instruction, Set[ProgramVariable[_]](variable))

  /**
    * A sequence of sets of program variables that describe which variables can influence the final value of the given set during execution of the given trace.
    */
  def liveVariables[I: InstructionSet](trace: Seq[I], variables: Set[ProgramVariable[_]]): Seq[Set[ProgramVariable[_]]] = {
    trace.foldRight(variables :: Nil) { case (instruction, postLive :: rest) => usedVariables(instruction, postLive) :: postLive :: rest }
  }

  private def toVariable(sid: SortedQId): TaggedVariable = {
    val (id, optIndex) = sid.id match {
      case SymbolId(SSymbol(id))        => (id, None)
      case SymbolId(ISymbol(id, index)) => (id, Some(index))
      // TODO: other Ids?
    }

    sid.sort match {
      case Array1BV(_, _)   => TaggedVariable[ArrayTerm[BVTerm]](id, sid.sort, optIndex)
      case BoolSort()       => TaggedVariable[BoolTerm](id, sid.sort, optIndex)
      case BitVectorSort(_) => TaggedVariable[BVTerm](id, sid.sort, optIndex)
      // TODO: other sorts
    }
  }

  private object isModified extends TypedMap.Callback[ProgramVariable, Expr, Boolean] {
    override def apply[T](variable: ProgramVariable[T], value: Expr[T]): Boolean = variable != value
  }

}

private trait TaggedVariable {
  type T
  val variable: VarTerm[T]
}

private object TaggedVariable {
  def apply[T0](id: String, sort: Sort, optIndex: Option[Int]): TaggedVariable = new TaggedVariable {
    override type T = T0
    override val variable: VarTerm[T0] = new VarTerm[T0](id, sort, optIndex)
  }
}

