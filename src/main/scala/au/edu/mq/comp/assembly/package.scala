package au.edu.mq.comp

import au.edu.mq.comp.assembly.semantics.Semantics

import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.Term
import org.bitbucket.franck44.scalasmt.theories.BoolTerm
import org.bitbucket.franck44.scalasmt.typedterms.{TypedTerm, VarTerm}

import scala.collection.IterableLike
import scala.util.{Failure, Success, Try}

package object assembly {

  // ***************************
  //  Type Aliases
  // ***************************

  // Shorthand for arbitrary terms.
  type Expr[T] = TypedTerm[T, _ <: Term]

  // A formula is a term of type bool.
  type Formula = Expr[BoolTerm]

  // Program variables have a different intuitive meaning,
  // but are expressed with the same type as logic variables.
  type ProgramVariable[T] = VarTerm[T]

  // A state predicate is a formula over program variables.
  type StatePredicate = Formula

  // Locations are represented as BigInts.
  type Location = BigInt

  // A trace is a sequence of instructions.
  // A labeled trace is a sequence of location-aware instructions.
  type LabeledTrace[I] = Seq[(Location, I)]


  // ***************************
  //  Guarded monad
  // ***************************

  /**
    * A monad combining a value with a guard SMT formula.
    *
    * @param guards A set of guard formulas. The value must only be used in contexts satisfying these formulas.
    * @param value  The value.
    * @tparam T The value's type.
    */
  case class Guarded[T](guards: Set[Formula], value: T) {
    def flatMap[S](f: T => Guarded[S]): Guarded[S] = {
      val Guarded(newGuards, newValue) = f(value)
      Guarded(guards ++ newGuards, newValue)
    }

    def map[S](f: T => S): Guarded[S] = Guarded(guards, f(value))

    def withFilter(condition: T => Formula): Guarded[T] = Guarded(guards + condition(value), value)
  }

  object Guarded {
    def apply[T](value: T): Guarded[T] = Guarded(Set.empty, value)

    implicit object GuardedMonad extends Monad[Guarded] {
      override def pure[A](x: A): Guarded[A] = Guarded(x)

      override def flatMap[A, B](m: Guarded[A], f: A => Guarded[B]): Guarded[B] = m.flatMap(f)
    }

  }

  implicit def guard[T](x: T): Guarded[T] = Guarded(x)

  def assert[T](condition: Formula)(body: Guarded[T]): Guarded[T] = {
    val Guarded(guards, value) = body
    Guarded(guards + condition, value)
  }

  // ***************************
  //  Typed Collections
  // ***************************

  trait TypedFunction[-T[_], +R[_]] {
    def apply[I](x: T[I]): R[I]
  }

  implicit class TypedMapKey[K[_], T](k: K[T]) {

    import au.edu.mq.comp.assembly.TypedMap.Void

    // General case: singleton map
    def |->[V[_]](v: V[T]): TypedMap[K, V] = new TypedMap[K, V] {
      override def apply[S](s: K[S]): V[S] = if (s == k) v.asInstanceOf[V[S]] else ???

      override def iterateKeys(fct: TypedFunction[K, Void]): Unit = fct(k)
    }

    // Specialized case, necessary sometimes because Expr is just a type alias
    def |->(v: Expr[T]): TypedMap[K, Expr] = k.|->[Expr](v)
  }

  // ***************************
  //  Extension methods
  // ***************************

  trait Monad[M[_]] {
    def pure[A](x: A): M[A]

    def flatMap[A, B](m: M[A], f: A => M[B]): M[B]
  }

  implicit object TryMonad extends Monad[Try] {
    override def pure[A](x: A): Try[A] = Success(x)

    override def flatMap[A, B](m: Try[A], f: A => Try[B]): Try[B] = m.flatMap(f)
  }

  implicit class CollectionExtensions[A, Repr](coll: IterableLike[A, Repr]) {

    import scala.collection.generic.CanBuildFrom

    def foldRightM[B, M[_]](seed: => B)(aggregate: (A, B) => M[B])(implicit m: Monad[M]): M[B] = {
      coll.foldRight(m.pure(seed)) {
        case (elem, acc) => m.flatMap(acc, aggregate(elem, _: B))
      }
    }

    def foldLeftM[B, M[_]](seed: => B)(aggregate: (B, A) => M[B])(implicit m: Monad[M]): M[B] =
      coll.foldLeft(m.pure(seed)) {
        case (acc, elem) => m.flatMap(acc, aggregate(_: B, elem))
      }

    def tryMapAll[B, That](f: A => Try[B])(implicit cbf: CanBuildFrom[Repr, B, That]): Try[That] = {
      val builder = cbf()
      coll.foreach { a =>
        f(a) match {
          case Success(b) => builder += b
          case Failure(e) => return Failure(e)
        }
      }
      Success(builder.result())
    }
  }

  implicit class OptionExtensions[A](opt: Option[A]) {
    def toTry(msg: String = "Option was empty."): Try[A] = opt.map(Success(_)).getOrElse(Failure(new Exception(msg)))
  }

  implicit class InstructionOps[I](instruction: I)(implicit instructionSet: InstructionSet[I]) {
    def semantics: Semantics = instructionSet.semantics(instruction)

    def print: String = instructionSet.print(instruction)
  }

  implicit class ExprOps[T](expr: Expr[T]) {


    import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.{SortedQId, Term}
    import org.bitbucket.inkytonik.kiama.rewriting.Rewriter.{alltd, rewrite, rule}

    /**
      * Performs variable substitution.
      * Assumes that the expression does not contain quantifiers.
      *
      * @param variable The variable to be substituted.
      * @param value    The expression replacing the variable.
      * @tparam S The logical type of variable and value.
      */
    def substitute[S](variable: VarTerm[S], value: Expr[S]): Expr[T] = {
      val substTerm = rewrite(alltd(rule[Term](Map(variable.termDef -> value.termDef))))(expr.termDef)
      new TypedTerm[T, Term](expr.typeDefs -- variable.typeDefs ++ value.typeDefs, substTerm)
    }

    /**
      * Performs variable substitution.
      * Assumes that the expression does not contain quantifiers.
      *
      * @param substitution A map from variables to values which replace them.
      */
    def substitute(substitution: TypedMap[VarTerm, Expr]): Expr[T] = {
      val (obsoleteDefs, newDefs) = substitution.map(new TypedMap.Callback[VarTerm, Expr, (Set[SortedQId], Set[SortedQId])] {
        override def apply[S](k: VarTerm[S], v: Expr[S]): (Set[SortedQId], Set[SortedQId]) = (k.typeDefs, v.typeDefs)
      }).unzip

      val termSubstitution = substitution.map(new TypedMap.Callback[VarTerm, Expr, (Term, Term)] {
        override def apply[S](k: VarTerm[S], v: Expr[S]): (Term, Term) = (k.termDef, v.termDef)
      }).toMap

      val substTerm = rewrite(alltd(rule[Term](termSubstitution)))(expr.termDef)
      new TypedTerm[T, Term](expr.typeDefs -- obsoleteDefs.flatten ++ newDefs.flatten, substTerm)
    }
  }

  // ***************************
  //  Functional helpers
  // ***************************

  /**
    * Returns a version of of the given function that caches the result for the respective parameters.
    */
  def memoize[A, B](f: A => B): A => B = new scala.collection.mutable.HashMap[A, B]() {
    self =>
    override def apply(a: A): B = self.synchronized(getOrElseUpdate(a, f(a)))
  }

  def memoize[A1, A2, B](f: (A1, A2) => B): (A1, A2) => B = memoize(f.tupled).untupled

  def memoize[A1, A2, A3, B](f: (A1, A2, A3) => B): (A1, A2, A3) => B = memoize(f.tupled).untupled3

  def memoizeTry[A, B](f: A => Try[B]): A => Try[B] = new scala.collection.mutable.HashMap[A, Try[B]]() {
    self =>
    override def apply(a: A): Try[B] = {
      self.synchronized {
        get(a) match {
          case Some(value) => value
          case None        =>
            val value = f(a)
            if (value.isSuccess)
              put(a, value)
            value
        }
      }
    }
  }

  def memoizeTry[A1, A2, B](f: (A1, A2) => Try[B]): (A1, A2) => Try[B] = memoizeTry(f.tupled).untupled

  implicit class FunctionOp[A, B](f: A => B) {
    def untupled[A1, A2](implicit ev: (A1, A2) =:= A): (A1, A2) => B = (x, y) => f((x, y))

    def untupled3[A1, A2, A3](implicit ev: (A1, A2, A3) =:= A): (A1, A2, A3) => B = (x, y, z) => f((x, y, z))
  }

  /**
    * Finds the fixed point of a function that may fail.
    *
    * @param initial The initial value of the fixed-point iteration.
    * @param f       The function whose fixed point is computed.
    * @tparam T The function's return type.
    * @return The fixed point of the function, or the first failure that occurs in the computation.
    */
  def tryFixedPoint[T](initial: T)(f: T => Try[T]): Try[T] = {
    lazy val iterations: Stream[Try[T]] = Success(initial) #:: iterations.map(_.flatMap(f))
    iterations.zip(iterations.tail)
      .collectFirst {
        case (Success(a), Success(b)) if a == b => Success(a)
        case (Failure(e), _)                    => Failure(e)
      }
      .get // either a failure occurs, or a fixed point is reached, or the search doesn't terminate -- hence an exception is never thrown here.
  }

}
