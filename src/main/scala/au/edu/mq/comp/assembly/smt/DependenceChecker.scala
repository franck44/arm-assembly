package au.edu.mq.comp.assembly
package smt

import au.edu.mq.comp.assembly.smt.Solving._
import au.edu.mq.comp.assembly.smt.Syntax._

import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics
import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
import org.bitbucket.franck44.scalasmt.typedterms.TypedTerm
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter.{everywhere, rewrite, rule}

import scala.util.Try

/**
  * Uses an SMT solver to check dependencies between logical terms and formulas
  * established by a set of logical constraints.
  *
  * @param smtLogic The SMT logic to use. It should contain all logical constraints and terms,
  *                 as well as simple quantified formulas derived from them.
  * @param smtCtx   An SMT context to use.
  */
final case class DependenceChecker(smtLogic: SMTLogics)(implicit smtCtx: SolverContext) {

  require(smtCtx.supportsLogic(smtLogic), s"Solver does not support logic $smtLogic.")

  /**
    * Detect if two expressions are interdependent or independent.
    *
    * @param frame A set of constraints that establishes the dependencies.
    * @param e1    The first expression.
    * @param e2    The second expression. The two expressions can have differing logical types.
    * @return True if a dependence between the given expressions was detected,
    *         false if they were found to be independent.
    *         None if neither result could be established.
    */
  def checkDependence[T1, T2](frame: Set[Formula], e1: Expr[T1], e2: Expr[T2]): Try[Boolean] = {
    smtCtx.withSolver(smtLogic) { implicit solver =>
      val frame1 = frame.map(instantiate(1))
      val frame2 = frame.map(instantiate(2))

      val instance1 = instantiate(1)(e1)
      val instance2 = instantiate(2)(e2)

      val variables = frame.flatMap(_.typeDefs) ++ e1.typeDefs ++ e2.typeDefs
      val dependenceFormula = forall(variables)(frame.conjunction implies (e1 =/= instance1 || e2 =/= instance2))

      for {
        _ <- frame1.tryMapAll(|=)
        _ <- frame2.tryMapAll(|=)
        _ <- |=(dependenceFormula)

        r <- checkSat() if r != UnKnown()
      } yield r == Sat()
    }
  }

  private def instantiate[T](instance: Int)(expr: Expr[T]): Expr[T] = {
    TypedTerm[T, Term](
      expr.typeDefs.map(instantiateTerm(instance)),
      instantiateTerm(instance)(expr.aTerm)
    )

  }

  private def instantiateTerm[T <: ASTNode](instance: Int)(term: T): T = {
    val suffix = "$$" + instance
    rewrite(everywhere(instantiationRule(suffix)))(term)
  }

  private def instantiationRule(suffix: String) = rule[SMTLIB2Symbol] {
    case DelimitedSeq(name)                                 => DelimitedSeq(name + suffix)
    case SSymbol(name) if name != "true" && name != "false" => SSymbol(name + suffix)
    case ISymbol(name, index)                               => ISymbol(name + suffix, index)
  }
}
