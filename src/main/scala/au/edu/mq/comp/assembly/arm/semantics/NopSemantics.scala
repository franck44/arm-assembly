package au.edu.mq.comp.assembly
package arm.semantics

import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax.Instruction
import au.edu.mq.comp.assembly.arm.syntax.PC
import au.edu.mq.comp.assembly.semantics._
import au.edu.mq.comp.assembly.smt.Syntax._

/** A simple semantics that does nothing but increase the PC by 4 (unconditionally). */
object NopSemantics extends SemanticsDefinition[Instruction] {

  override def apply(i: Instruction): Semantics = semantics

  val semantics: Semantics = new Semantics {
    override val isDefined: Set[StatePredicate] = Set.empty
    override val update: StateUpdate = PC().asVar |-> (PC().asVar + 4.ui32)
  }
}
