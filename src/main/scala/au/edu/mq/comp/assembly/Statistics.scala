package au.edu.mq.comp.assembly

import au.edu.mq.comp.assembly.controlflow.{ControlFlowGraph, PredicateAutomatonResolver, Resolver}

import org.bitbucket.franck44.automat.util.{DFSVisitor, Traversal}


object Statistics {

  import scala.collection.mutable

  private var pgmSource: String = ""
  private var pgmSize: Int = -1
  private var activeMeasurements: mutable.Map[String, Long] = new mutable.HashMap[String, Long]()
  private var pastMeasurements: mutable.Map[String, Long] = new mutable.HashMap[String, Long]()
  private var resolverData: mutable.Map[String, ResolverData] = new mutable.HashMap[String, ResolverData]()
  private var cfgStates: Int = -1
  private var cfgDupl: Int = -1
  private var refinementIterations: Int = 0
  private var resolutions: Int = 0

  def clear(): Unit = {
    pgmSource = ""
    pgmSize = -1
    activeMeasurements.clear()
    pastMeasurements.clear()
    resolverData.clear()
    cfgStates = -1
    cfgDupl = -1
    refinementIterations = 0
    resolutions = 0
  }

  def startMeasure(id: String): Unit = activeMeasurements.put(id, System.nanoTime)

  def stopMeasure(id: String): Unit = {
    val start = activeMeasurements(id)
    activeMeasurements.remove(id)
    pastMeasurements.put(id, System.nanoTime - start)
  }

  def countRefinement(): Unit = {
    refinementIterations = refinementIterations + 1
  }

  def countResolution(): Unit = {
    resolutions = resolutions + 1
  }

  def inspectProgram(source: String, program: Program[_]): Unit = {
    pgmSource = source
    pgmSize = program.instructionAt.size
  }

  def inspectResolver(id: String, resolver: Resolver[_]): Unit = resolverData.put(id, ResolverData.create(resolver))

  def inspectCFG(cfg: ControlFlowGraph[_, _]): Unit = {
    val duplLoc = cfg.states.groupBy(cfg.location).mapValues(_.size).count { case (_, i) => i > 1 }
    cfgStates = cfg.states.size
    cfgDupl = duplLoc
  }

  import java.io.File

  def write(file: File): Unit = {
    import java.io.FileWriter

    val measurements = pastMeasurements
      .toSeq.sortBy(_._1)
      .map(_._2).mkString(",")
    val resolvers = resolverData
      .toSeq.sortBy(_._1)
      .map { case (_, data) => s"${data.states},${data.transitions},${data.multiLocationStates}" }
      .mkString(",")

    val headerLine = if (!file.exists) header else ""
    val line = s"$pgmSource,$pgmSize,$cfgStates,$cfgDupl,$resolutions,$refinementIterations,$measurements,$resolvers\n"

    val writer = new FileWriter(file, true)
    writer.write(headerLine + line)
    writer.flush()
    writer.close()
  }

  private def header: String = {
    val measurements = pastMeasurements
      .toSeq.sortBy(_._1)
      .map(_._1).mkString(",")
    val resolvers = resolverData
      .toSeq.sortBy(_._1)
      .map { case (id, _) => s"# states ($id),# transitions ($id), # multi-loc states ($id)" }
      .mkString(",")

    s"file,# program locations,# CFG states,# duplicate locations,# resolved nodes,# iterations,$measurements,$resolvers\n"
  }
}


final case class ResolverData private(states: Int, transitions: Int, multiLocationStates: Int)

object ResolverData {
  def create[I](resolver: Resolver[I]): ResolverData = {
    val data = Traversal.DFS(
      SizeVisitor[resolver.State, I](resolver.label(_: resolver.State).exists(_.size > 1)),
      resolver.dfa.outGoingEdges(_: resolver.State)
    )(resolver.dfa.getInit)

    ResolverData(
      data.states,
      data.edges,
      data.multiStates
    )
  }

  private case class SizeVisitor[S, L](isMulti: S => Boolean, states: Int = 0, edges: Int = 0, multiStates: Int = 0) extends DFSVisitor[S, L, SizeVisitor[S, L]] {
    override def discoverState(s: S, discoveryTime: Map[S, Int], parent: Map[S, (S, L)]): SizeVisitor[S, L] = copy(
      states = states + 1,
      multiStates = if (isMulti(s)) multiStates + 1 else multiStates
    )

    override def treeEdge(s: S, l: L, t: S, discoveryTime: Map[S, Int], parent: Map[S, (S, L)]): SizeVisitor[S, L] = copy(edges = edges + 1)

    override def backEdge(s: S, l: L, t: S, discoveryTime: Map[S, Int], parent: Map[S, (S, L)]): SizeVisitor[S, L] = copy(edges = edges + 1)

    override def forwardEdge(s: S, l: L, t: S, discoveryTime: Map[S, Int], parent: Map[S, (S, L)]): SizeVisitor[S, L] = copy(edges = edges + 1)
  }
}