package au.edu.mq.comp.assembly
package controlflow
package resolution

import au.edu.mq.comp.assembly.smt.Solving.{checkSat, getInterpolants, |=}
import au.edu.mq.comp.assembly.smt.Syntax._
import au.edu.mq.comp.assembly.smt.{SolverContext, TraceEncoder, ValueCollector, Z3Normalizer}

import org.bitbucket.franck44.scalasmt.configurations.SMTOptions.INTERPOLANTS
import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.{NamedTerm, UnSat}
import org.bitbucket.franck44.scalasmt.theories.BoolTerm
import org.bitbucket.franck44.scalasmt.typedterms.Named

import scala.util.Try

/**
  * Resolves successors using an SMT solver and interpolant generation.
  *
  * @param valueCollector   Used to collect possible location values after execution of a trace.
  * @param traceEncoder     Used to encode a trace as SMT formulas.
  * @param automatonBuilder Used to build a resolver automaton from the computed interpolants.
  * @param smtCtx           Used to compute interpolants.
  * @tparam I1 The type of instructions in the underlying program.
  * @tparam I2 The type of instructions in a trace. There must be a supporting instruction set.
  */
case class InterpolantsSuccessorResolution[I1, I2](
                                                    valueCollector: ValueCollector,
                                                    traceEncoder: TraceEncoder[I1, (Location, I2)],
                                                    automatonBuilder: PredicateAutomatonBuilder[(Location, I2)]
                                                  )(
                                                    implicit smtCtx: SolverContext, instructionSet: InstructionSet[I2]
                                                  )
  extends SuccessorResolution[I1, I2]
    with Logging {

  import instructionSet.machine.{expressionToLocation, pcExpr, smtLogic}

  require(smtCtx.supportsOption(INTERPOLANTS), "Solver does not support interpolation.")
  require(smtCtx.supportsLogic(smtLogic), s"Solver does not support logic $smtLogic.")

  override def resolve(program: Program[I1], alphabet: Set[(Location, I2)], existing: PredicateAutomatonResolver[(Location, I2)])
                      (trace: LabeledTrace[I2], parallel: Seq[Set[(Location, I2)]]): Try[PredicateAutomatonResolver[(Location, I2)]] = {

    for {
      (successors, interpolants) <- determineSuccessors(program, trace)
      automaton = automatonBuilder.buildPredicateAutomaton(trace, interpolants.map(_.unIndexed))(parallel, alphabet, existing.hasEdge)
    } yield PredicateAutomatonResolver.create(automaton, successors)
  }

  /// ----------------------------------
  /// some private convenience accessors
  /// ----------------------------------
  private type NamedFormula = Named[BoolTerm, NamedTerm]

  /**
    * Determines possible locations after execution of the given trace.
    *
    * @return A set of locations, and (if possible) a list of interpolants proving that there are no other locations.
    */
  private def determineSuccessors(program: Program[I1], trace: LabeledTrace[I2]): Try[(Set[Location], List[Formula])] = {
    require(trace.nonEmpty, "Successor and interpolant computation not possible for empty trace.")
    logInstructions("trace:", trace)

    for {
      // Encode trace as formulas..
      encoding <- traceEncoder.encode(program, trace, smtLogic)

      _ = logFormulas("initial state:", encoding.initial.toSeq)
      _ = logFormulas("trace encoding:", encoding.traceFormulas)

      // Compute successor locations.
      finalLocation = encoding.states.last.substitute(pcExpr)
      constants <- valueCollector.getValues(encoding.formulas, finalLocation, smtLogic)
      successors <- constants.tryMapAll(expressionToLocation)

      _ = logger.debug(s"found successors: { ${successors.map(_.toString(16)).mkString(", ")} }")

      // Compute interpolants.
      finalPredicate = constants.map(finalLocation =/= _).conjunction.named("finalPredicate")
      formulaSequence = encoding.initial.conjunction.named("initial") +:
        (encoding.traceFormulas.zipWithIndex.map { case (f, i) => f.named(s"instruction$i") } :+
          finalPredicate)

      itp <- computeInterpolants(formulaSequence.toList)

    } yield (successors, itp)
  }

  private def computeInterpolants(formulas: List[NamedFormula]): Try[List[Formula]] = {
    require(formulas.size > 1, "Cannot compute interpolants for single formula.")
    logFormulas("Formulas for which interpolants are computed:", formulas)

    smtCtx.withSolver(smtLogic, INTERPOLANTS) { implicit solver =>
      for {
        _ <- formulas.tryMapAll(|=)
        s <- checkSat() if s == UnSat()

        itp <- getInterpolants(formulas.head, formulas.tail.head, formulas.drop(2): _*)
      } yield itp.map(Z3Normalizer.normalize(_))
    }
  }
}
