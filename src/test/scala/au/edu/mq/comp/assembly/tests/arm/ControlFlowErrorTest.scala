package au.edu.mq.comp.assembly.tests.arm

import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax.Instruction

class ControlFlowErrorTest extends TestSpec {
  private lazy val testSuites = Table(
    ("file", "tests"),
    ("decision.s", Table(
      ("trace", "expected"),
      (List(0x0, 0x4, 0x8, 0x20), Result.Feasible),
      (List(0x0, 0x8, 0x20), Result.Feasible),
      (List(0x0, 0x4, 0x20), Result.CFE)
    )),
    ("double-call.s", Table(
      ("trace", "expected"),
      (List(0x0, 0x20, 0x4, 0x20, 0x8), Result.Feasible),
      (List(0x0, 0x20, 0x8), Result.CFE),
      (List(0x0, 0x20, 0x4, 0x20, 0x4), Result.CFE),
      (List(0x0, 0x4), Result.CFE)
    )),
    ("double-diamond.s", Table(
      ("trace", "expected"),
      (List(0x0, 0x4, 0x8, 0xc, 0x10), Result.Feasible),
      (List(0x8, 0x10), Result.Feasible),
      (List(0x0, 0x4, 0x8, 0x10), Result.InfeasibleNoCFE),
      (List(0x0, 0x8, 0xc, 0x10), Result.InfeasibleNoCFE)
    )),
    ("loop-exit.s", Table(
      ("trace", "expected"),
      (List(0x0, 0x4, 0x20), Result.Feasible),
      (List(0x0, 0x0, 0x0, 0x0), Result.Feasible),
      (List(0x0, 0x0, 0x4, 0x20), Result.InfeasibleNoCFE),
      (List(0x0, 0x4, 0x0), Result.CFE)
    )),
    ("recursion.s", Table(
      ("trace", "expected"),
      (List(0x0, 0x20, 0x30, 0x4), Result.Feasible),
      (List(0x0, 0x20, 0x24, 0x28, 0x20, 0x30, 0x2c, 0x30, 0x4), Result.InfeasibleNoCFE),
      (List(0x0, 0x20, 0x30, 0x20), Result.CFE)
    ))
  )

  ignore("CFE definition behaves as expected") {
    forAll(testSuites) { case (file, tests) =>
      val program = parseProgram(file) // TODO: pass correct file
      forAll(tests) { case (trace, expected) =>
        val realTrace = program.materializeTrace(trace.map(BigInt(_)))
        check(realTrace) should be(expected)
      }
    }
  }

  private def check(trace: Seq[Instruction]): Result = ???
}

private trait Result

private object Result {

  case object Feasible extends Result

  case object CFE extends Result

  case object InfeasibleNoCFE extends Result

}
