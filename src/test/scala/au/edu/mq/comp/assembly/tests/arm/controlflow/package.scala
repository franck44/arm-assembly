package au.edu.mq.comp.assembly.tests.arm

import au.edu.mq.comp.assembly.InstructionSet
import au.edu.mq.comp.assembly.arm.AArch32
import au.edu.mq.comp.assembly.arm.semantics.UnconditionalInstruction
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax.Instruction

package object controlflow {
  implicit val baseInstructionSet: InstructionSet[Instruction] = AArch32
  implicit val unconditionalInstructionSet: InstructionSet[UnconditionalInstruction] = AArch32.Unconditional
}
