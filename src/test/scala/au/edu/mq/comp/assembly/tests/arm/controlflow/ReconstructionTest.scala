package au.edu.mq.comp.assembly.tests.arm.controlflow

import au.edu.mq.comp.assembly.arm.ArmProcessor
import au.edu.mq.comp.assembly.arm.semantics.{ConditionalInstruction, UnconditionalInstruction}
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax.Instruction
import au.edu.mq.comp.assembly.controlflow._
import au.edu.mq.comp.assembly.controlflow.resolution.{InterpolantsSuccessorResolution, SuccessorResolution, UniqueSuccessorResolution, WpSuccessorResolution}
import au.edu.mq.comp.assembly.smt._
import au.edu.mq.comp.assembly.tests.arm.TestSpec
import au.edu.mq.comp.assembly.{Guarded, InstructionSet, Location, Logging}

import org.bitbucket.franck44.scalasmt.configurations.SolverConfig
import org.scalatest.Ignore

abstract class ReconstructionTest[I2: InstructionSet](
                                                       identifier: String,
                                                       optimizeSimple: Boolean = true,
                                                       concretize: Instruction => Set[I2]
                                                     )
  extends TestSpec with Logging {

  import au.edu.mq.comp.assembly.arm.controlflow.SuccessorAnalysis

  import org.scalatest.Tag
  import org.scalatest.time.Span
  import org.scalatest.time.SpanSugar._

  import scala.concurrent.duration.Duration
  import scala.language.postfixOps

  import java.io.File
  import java.util.concurrent.{ExecutorService, Executors}

  /// ********************************************
  /// Properties to be overridden by subclasses.
  /// ********************************************
  protected implicit def smtConfig: SolverConfig

  protected implicit lazy val smtContext: SolverContext = FreshSolver(smtConfig)

  protected def resolution: SuccessorResolution[Instruction, I2]

  protected def simplifier: Simplifier = NoSimplifier

  /// ********************************************
  /// Tags used for tests.
  /// ********************************************
  private lazy val simplifierTag   = Tag(simplifier.getClass.getSimpleName)
  private lazy val optimizationTag = if (optimizeSimple) Tag("optimized") else Tag("unoptimized")
  private lazy val solverTag       = Tag(smtConfig.name)
  private lazy val resolutionTag   = resolution match {
    case InterpolantsSuccessorResolution(_, _, _) => Tag("interpolants")
    case WpSuccessorResolution(_, _, _, _)        => Tag("wp")
  }
  private lazy val encoderTag      = resolution match {
    case InterpolantsSuccessorResolution(_, e, _) => Tag(e.getClass.getSimpleName)
    case WpSuccessorResolution(_, e, _, _)        => Tag(e.getClass.getSimpleName)
  }

  /// ********************************************
  /// Tests.
  /// ********************************************
  benchmarks.foreach { case (set, files) =>
    //Thread.sleep(5000)
    files.foreach { file =>
      property(s"reconstructs CFG for $file", Tag("reconstruction"), Tag(set), simplifierTag, optimizationTag, resolutionTag, solverTag, encoderTag) {
        logger.info(s"Parsing $file")
        val program = parseProgram(file.getAbsolutePath)

        logger.info(s"Analysing $file (timeout ${timeout(set).prettyString})")
        val cfg = cancelAfter(timeout(set))(reconstruction.reconstruct(program)).get

        logger.info(s"Completed reconstruction. Writing CFG to file.")
        write(cfg, new File(s"${file.getAbsolutePath}-$identifier.dot"))
      }
    }
  }

  /// ********************************************
  /// Helper fields used by tests.
  /// ********************************************
  private lazy val effectiveResolution = if (optimizeSimple)
    resolution
  else
    UniqueSuccessorResolution(SuccessorAnalysis[I2](simplifier), PredicateAutomatonBuilder[(Location, I2)]()) +> resolution

  private lazy val effectiveAnalysis: UniqueSuccessorAnalysis[I2] = if (optimizeSimple)
    SuccessorAnalysis(simplifier)
  else
    UniqueSuccessorAnalysis.Dummy()

  private lazy val reconstruction: Reconstruction[Instruction, I2] = {
    Reconstruction(
      effectiveResolution,
      effectiveAnalysis,
      concretize
    )
  }

  /// ********************************************
  /// Time limit management for tests.
  /// ********************************************
  private val timeout: Map[String, Span] = Map(
    "custom" -> (2 minutes),
    "malardalen" -> (15 minutes)
  )

  private val pool: ExecutorService = Executors.newSingleThreadExecutor()

  // scalatest's cancelAfter method does not reliably cancel if the main thread is busy.
  // Hence this custom implementation.
  private def cancelAfter[T](duration: Duration)(value: => T): T = {
    import scala.concurrent.TimeoutException

    import java.util.concurrent.TimeUnit

    val computation = pool.submit[T](() => value)
    try {
      computation.get(duration.toMillis, TimeUnit.MILLISECONDS)
    } catch {
      case _: TimeoutException => cancel(s"Test timed out after $duration")
    } finally {
      computation.cancel(true)
    }
  }

  /// ********************************************
  /// Test output.
  /// ********************************************
  private def write[I: InstructionSet](cfg: ControlFlowGraph[_, I], file: File): Unit = {
    import au.edu.mq.comp.assembly.{InstructionOps, Location}

    import org.bitbucket.franck44.automat.util.DotConverter.toDot
    import org.bitbucket.franck44.dot.DOTPrettyPrinter.show
    import org.bitbucket.franck44.dot.DOTSyntax.{Attribute, StringLit}

    import java.io.PrintWriter

    val dot = toDot(
      cfg.toLabeledNFA,
      nodeDotName  = (x: cfg.State) => s""""$x"""",
      nodeProp     = (x: cfg.State) => List(Attribute("label", StringLit(f"${cfg.location(x)}%04X")), Attribute("tooltip", StringLit(x.toString))),
      labelDotName = (i: (Location, I)) => i.print
    )

    val writer = new PrintWriter(file)
    writer.print(show(dot))
    writer.flush()
    writer.close()
  }
}

class OldItpZ3ReconstructionTest extends ReconstructionTest("Z3-itp-old", optimizeSimple = false, concretize = ConditionalInstruction.concretize) {
  protected override implicit lazy val smtConfig: SolverConfig = solver("Z3")

  protected override lazy val simplifier: Simplifier = Z3Simplifier

  protected override lazy val resolution: SuccessorResolution[Instruction, Guarded[UnconditionalInstruction]] =
    InterpolantsSuccessorResolution(ValueCollector(), DefaultTraceEncoder(), PredicateAutomatonBuilder())
}

class OptimizedItpZ3ReconstructionTest extends ReconstructionTest("Z3-itp-opt", concretize = ConditionalInstruction.concretize) {
  protected override implicit lazy val smtConfig: SolverConfig = solver("Z3")

  protected override lazy val simplifier: Simplifier = Z3Simplifier

  protected override lazy val resolution: SuccessorResolution[Instruction, Guarded[UnconditionalInstruction]] =
    InterpolantsSuccessorResolution(ValueCollector(), DefaultTraceEncoder(), PredicateAutomatonBuilder())
}

class ProjectedOptimizedItpZ3ReconstructionTest extends ReconstructionTest("Z3-itp-opt-proj", concretize = ConditionalInstruction.concretize) {
  protected override implicit lazy val smtConfig: SolverConfig = solver("Z3")

  protected override lazy val simplifier: Simplifier = Z3Simplifier

  protected override lazy val resolution: SuccessorResolution[Instruction, Guarded[UnconditionalInstruction]] =
    InterpolantsSuccessorResolution(ValueCollector(), SimpleProjectingTraceEncoder(DefaultTraceEncoder(), ArmProcessor.pcExpr), PredicateAutomatonBuilder())
}

class OptimizedWpZ3ReconstructionTest extends ReconstructionTest("Z3-wp-opt", concretize = ConditionalInstruction.concretize) {
  protected override implicit lazy val smtConfig: SolverConfig = solver("Z3")

  protected override lazy val simplifier: Simplifier = Z3Simplifier

  protected override lazy val resolution: SuccessorResolution[Instruction, Guarded[UnconditionalInstruction]] =
    WpSuccessorResolution(ValueCollector(), DefaultTraceEncoder(), PredicateAutomatonBuilder())
}

class OptimizedWpCvc4ReconstructionTest extends ReconstructionTest("CVC4-wp-opt", concretize = ConditionalInstruction.concretize) {
  protected override implicit lazy val smtConfig: SolverConfig = solver("CVC4")

  protected override lazy val resolution: SuccessorResolution[Instruction, Guarded[UnconditionalInstruction]] =
    WpSuccessorResolution(ValueCollector(), DefaultTraceEncoder(), PredicateAutomatonBuilder())
}

class ProjectedOptimizedWpCvc4ReconstructionTest extends ReconstructionTest("CVC4-wp-opt-proj", concretize = ConditionalInstruction.concretize) {
  protected override implicit lazy val smtConfig: SolverConfig = solver("CVC4")

  protected override lazy val resolution: SuccessorResolution[Instruction, Guarded[UnconditionalInstruction]] =
    WpSuccessorResolution(ValueCollector(), SimpleProjectingTraceEncoder(DefaultTraceEncoder(), ArmProcessor.pcExpr), PredicateAutomatonBuilder(), NoSimplifier)
}

class CompleteConfigurationReconstructionTest extends ReconstructionTest("complete", concretize = ConditionalInstruction.concretize) {
  private lazy val boolector = solver("Boolector")
  private lazy val yices = solver("Yices")

  protected override implicit lazy val smtConfig: SolverConfig = boolector

  //protected override implicit lazy val smtContext: SolverContext = SolverCache(boolector)

  protected override lazy val resolution: SuccessorResolution[Instruction, Guarded[UnconditionalInstruction]] =
    WpSuccessorResolution(
      ValueCollector(),
      ConstrainedSSATraceEncoder(SimpleProjectingTraceEncoder(DefaultTraceEncoder(), ArmProcessor.pcExpr)),
      PredicateAutomatonBuilder(),
      Z3Simplifier
    )
}

@Ignore // MathSAT has unexpected behaviour for let-expressions. Disable for now.
class OptimizedWpMathsatReconstructionTest extends ReconstructionTest("MathSat-wp-opt", concretize = ConditionalInstruction.concretize) {
  protected override implicit lazy val smtConfig: SolverConfig = solver("MathSat")

  protected override lazy val resolution: SuccessorResolution[Instruction, Guarded[UnconditionalInstruction]] =
    WpSuccessorResolution(ValueCollector(), DefaultTraceEncoder(), PredicateAutomatonBuilder())
}