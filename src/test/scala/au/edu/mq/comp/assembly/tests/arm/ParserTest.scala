package au.edu.mq.comp.assembly.tests.arm

import au.edu.mq.comp.assembly.arm.syntax.ARM

import org.bitbucket.inkytonik.kiama.util.{FileSource, Positions}

import java.io.File

class ParserTest extends TestSpec {
  property("can parse benchmark examples") {
    forEvery(benchmarkFiles) { armFile: File =>
      val p = parser(armFile)
      val result = p.pProgram(0)
      p.value(result) // cause exception if parse failed
    }
  }

  private def parser(file: File) = new ARM(FileSource(file.getAbsolutePath), new Positions)
}
