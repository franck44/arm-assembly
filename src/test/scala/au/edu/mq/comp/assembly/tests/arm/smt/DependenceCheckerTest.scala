package au.edu.mq.comp.assembly.tests.arm.smt

import au.edu.mq.comp.assembly.Formula
import au.edu.mq.comp.assembly.smt.{DependenceChecker, FreshSolver, SolverContext}
import au.edu.mq.comp.assembly.smt.Syntax._
import au.edu.mq.comp.assembly.tests.arm.TestSpec

import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics

class DependenceCheckerTest extends TestSpec {

  private val x = BVs("x", 32)
  private val y = BVs("y", 32)
  private val z = BVs("z", 32)
  private val x1 = BVs("x1", 32)

  private val arr = ArrayBV1("a", 32, 32)
  private val brr = ArrayBV1("b", 32, 32)

  private val frame = Set[Formula](
    x === y,
    x ugt z,
    x1 ugt z,

    arr.at(y) =/= arr.at(arr.at(z)),
    brr.store(z, z).at(x1) === arr.at(x1),
    brr.at(0.i32) ugt x
  )

  private val pairs = Table(
    ("e1", "e2", "expected"),
    (x, y, true),
    (y, x, true),
    (x, z, true),
    (z, y, true),
    (x, x1, false),
    (x1, z, false),

    (x, y ult z, false), // due to (y ult z) being constantly false
    (y === x1, y + x === x1, true),

    (x, arr.at(3.i32), false),
    (x, brr.at(x), true),
    (brr.at(z), arr.store(z, y).at(z), true),
    (brr, x, true),
    (brr, z, true),
    (brr, x1, false)
  )

  private lazy val z3: SolverContext = FreshSolver(solver("Z3"))
  private lazy val z3Logic: SMTLogics = ??? //SMTLogics.ALL
  private lazy val z3Checker = DependenceChecker(z3Logic)(z3)

  // Currently not working due to insufficient SMT solver support (gives "unknown" in most cases).
  ignore("correctly detects dependencies and independencies using Z3") {
    forEvery(pairs) { (e1, e2, expected) =>
      z3Checker.checkDependence(frame, e1, e2).get should be(expected)
    }
  }
}
