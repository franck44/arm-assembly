FROM franck44/docker-scala-sbt:latest

RUN apt-get update \
  && apt-get install -y gcc-arm-none-eabi git

WORKDIR app

COPY scripts/ scripts/
RUN scripts/install-z3.sh 4.5.0 \
  && scripts/install-cvc4.sh 1.4 \
  && scripts/install-yices.sh 2.6.0 \
  && scripts/install-boolector.sh 3.0.0

COPY src/ src/
COPY project/build.properties project/build.properties
COPY project/plugins.sbt project/plugins.sbt
COPY application.conf application.conf
COPY build.sbt build.sbt
RUN scripts/compile-benchmarks.sh

ENV PATH "$PATH:/root/app/solvers/boolector/3.0.0/bin:/root/app/solvers/cvc4/1.4/:/root/app/solvers/yices/2.6.0/bin/:/root/app/solvers/z3/4.5.0/bin/"
ENV SBT_OPTS "-XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=2G -Xmx3096M -Xss512m"

CMD ["sbt"]
